# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.256');

# ---------------------------------------------------------------------- #
# Modify Table service_provider                                          #
# ---------------------------------------------------------------------- # 
ALTER TABLE `service_provider` ADD COLUMN `BookingCapacityLimit` INT(3) NOT NULL DEFAULT '75' AFTER `ServiceProviderType`; 

# ---------------------------------------------------------------------- #
# Modify Table service_provider_engineer                                 #
# ---------------------------------------------------------------------- # 
ALTER TABLE `service_provider_engineer` ADD COLUMN `AvarageAppPerHour` DECIMAL(10,2) NOT NULL DEFAULT '1.25' AFTER `PrimarySkill`;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.257');
