# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.190');

# ---------------------------------------------------------------------- #
# Add table "service_provider"                                           #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider` ADD COLUMN `DiaryAllocationOnly` ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER `Multimaps`;
ALTER TABLE `service_provider` ADD COLUMN `Acronym` VARCHAR(50) NULL DEFAULT NULL AFTER `CompanyName`;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.191');
