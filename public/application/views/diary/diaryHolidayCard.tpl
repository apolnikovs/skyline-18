<head>
    <script>
   function  allDayEvent(){
    $('#hStartTime').val('00:00');
    $('#hEndTime').val('23:59');
    
    }
    function validateHolidayForm(f){
    if($('#hEng').val()!="000"){
        if($('#hStartDate').val()!=""&&$('#hEndDate').val()!=""&&$('#hStartTime').val()!=""&&$('#hEndTime').val()!=""){
        $.post("{$_subdomain}/Diary/validateHolidayAppointment/", $("#holidayCardForm").serialize(),function(data){
            if (data.status == "OK") {
                f.submit();
                 $.colorbox({ html:$('#waitDiv').html(), title:"",escKey: false,
                overlayClose: false});
                $('#cboxClose').remove();
            } else {
                $("#secondaryDivText").html('<b>'+data.message+'</b>');
                $("#mainDiv").hide();
                $("#secondaryDiv").show();
                $(this).colorbox.resize();
                //alert(data.message);
            }
        },"json").error(function() { alert('Internal Server Error') ;});
 
        }else{
            alert('Please Select Start/Finish Date and Time');
        }
        }else{
            alert('Please select engineer');
    }
    }
    function checkTime(c){
if($('#hStartDate').val()!=""&&$('#hEndDate').val()!=""){
if(process($('#hStartDate').val())>process($('#hEndDate').val())){
if(c==2){
$('#hEndDate').val("");
}else{
$('#hStartDate').val("");
}
alert('The finish time you have selected is prior to start time. Please adjust the times and try again.');

}
}
}

function process(date){
   var parts = date.split("/");
   return new Date(parts[2], parts[1] - 1, parts[0]);
}
    </script>
    <body>
    
        <fieldset>            
            <legend>Engineer Absent Diary</legend>
            <p>The Skyline diary will reference these entries during optimisation</p>
            <div id="mainDiv"><form name="holidayCardForm" id ="holidayCardForm" method="post" action="{$_subdomain}/Diary/insertHoliday">
            {if isset($data)}<input type="hidden" name="id" value="{$data['DiaryHolidayID']}">{/if}
           
            <table>
                <tr>
                    <td style="background:none">
                        Engineer:
                    </td>
                    <td style="background:none">
                        <select name="hEng" id="hEng">
                            <option value="000">Please select engineer</option>
                        {foreach $engineerList as $ff}
                              <option {if isset($data)&&$data['ServiceProviderEngineerID']==$ff.ServiceProviderEngineerID}selected=selected{/if}   value="{$ff.ServiceProviderEngineerID}">{if isset($ff.EngineerFirstName)}{$ff.EngineerFirstName}{/if}{if isset($ff.EngineerLastName)} {$ff.EngineerLastName}{/if}</option>
                           {/foreach}
                        </select>
                    </td>
                
                </tr>
                <tr >
                    <td style="background:none">
                        Start Time:
                    </td>
                    <td style="background:none">
                        <input readonly=readonly onchange="checkTime(1);" value="{if isset($data)}{$data['StartTime']|date_format:"%d/%m/%Y"}{/if}"  type="text" name="hStartDate" id="hStartDate">
                        <input readonly=readonly value="{if isset($data)}{$data['StartTime']|date_format:"%H:%M"}{/if}" type="text" name="hStartTime" id="hStartTime">
                        <input type="checkbox" name="hAllDay" id="hAllDay" onclick="allDayEvent()"> All Day Event
                    </td>
                </tr>
                <tr>
                    <td style="background:none">
                        End Time:
                    </td>
                    <td style="background:none">
                        <input readonly=readonly onchange="checkTime(2);" value="{if isset($data)}{$data['EndTime']|date_format:"%d/%m/%Y"}{/if}" type="text" name="hEndDate" id="hEndDate">
                        <input readonly=readonly value="{if isset($data)}{$data['EndTime']|date_format:"%H:%M"}{/if}" type="text" name="hEndTime" id="hEndTime">
                       
                    </td>
                </tr>
                <tr>
                    <td style="background:none">
                        Reason:
                    </td>
                    <td style="background:none">
                        <textarea id="hReason" name="hReason" style="width: 400px; height:150px;">{$data['Reason']|default:""}</textarea>
                    </td>
                </tr>
                {if isset($data)}
                <tr>
                     <td style="background:none">
                        Created Date:
                    </td>
                    <td style="background:none">
                        {$data['Created']|date_format:'%d/%m/%Y (%H:%S)'}
                    </td>
                </tr>
                <tr>
                     <td style="background:none">
                        Created by:
                    </td>
                    <td style="background:none">
                        {$data['CompanyName']} {$data['Acronym']}
                    </td>
                </tr>
                {/if}
            </table>
            <div style="float:right">
                <button type="button" onclick="validateHolidayForm(this.form)" class="btnStandard">{if isset($data)}Update{else}Insert{/if}</button>
                <button type="button" onclick="$.colorbox.close()" class="btnStandard">Cancel</button>
            </div>
                </form></div>
                <div id="secondaryDiv" style="display: none; text-align: center;">
                    <span id="secondaryDivText"></span><br><br>
                    <button type="button" onclick="location.reload();" class="btnStandard">Ok</button>
                </div>
        </fieldset>
 <div id="waitDiv" style="display:none">
     <div style="min-height:100px;min-width: 100px;">
        <img src="{$_subdomain}/images/processing.gif">
        <div style="text-align: center">Please wait!</div></div> </div>   
    </body>

</head>