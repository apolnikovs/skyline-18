# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.139');


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_skillset_day"                  #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer_skillset_day` ADD INDEX ( `ServiceProviderEngineerDetailsID` );


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer_details"                       #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer_details` ADD INDEX ( `ServiceProviderEngineerID` , `WorkDate` );


# ---------------------------------------------------------------------- #
# Modify table "appointment_allocation_slot"                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment_allocation_slot` ADD `Colour` VARCHAR( 10 ) NULL;


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider_engineer` ADD `MobileNumber` VARCHAR( 20 ) NULL AFTER `EmailAddress`;


# ---------------------------------------------------------------------- #
# Modify table "service_provider"                                        #
# ---------------------------------------------------------------------- #

ALTER TABLE `service_provider` ADD `AutoSelectDay` ENUM( '', 'Today', 'NextDay' ) NULL DEFAULT '';
ALTER TABLE `service_provider` ADD `AutoDisplayTable` ENUM( '', 'Summary', 'Appointment' ) NULL DEFAULT '';
ALTER TABLE `service_provider` ADD `AutoSpecifyEngineerByPostcode` ENUM( 'Yes', 'No' ) NULL DEFAULT 'No'; 



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.140');






