# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.291');

# ---------------------------------------------------------------------- #
# Raju Changes     														 #
# ---------------------------------------------------------------------- # 
ALTER TABLE repair_type ADD ServiceProviderID INT NOT NULL AFTER RepairType; 
ALTER TABLE repair_type ADD isDeleted ENUM( 'Yes', 'No' ) NOT NULL DEFAULT 'No' AFTER Status; 
ALTER TABLE supplier ADD ShipToAccountNumber VARCHAR( 50 ) NULL AFTER AccountNumber;


# ---------------------------------------------------------------------- #
# Thirumalesh Changes     												 #
# ---------------------------------------------------------------------- # 
CREATE TABLE IF NOT EXISTS `group_headings` (
  `GroupID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupHeading` varchar(60) DEFAULT NULL,
  `Tooltip` varchar(30) NOT NULL,
  `Description` text,
  `CreatedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `EndDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` enum('Active','In-active') NOT NULL DEFAULT 'Active',
  `ModifiedUserID` int(11) DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Code` varchar(30) NOT NULL,
  `ParentID` int(2) DEFAULT NULL,
  `URLSegment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`GroupID`),
  KEY `IDX_permission_ModifiedUserID_FK` (`ModifiedUserID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;


INSERT INTO `group_headings` (`GroupID`, `GroupHeading`, `Tooltip`, `Description`, `CreatedDate`, `EndDate`, `Status`, `ModifiedUserID`, `ModifiedDate`, `Code`, `ParentID`, `URLSegment`) VALUES
(1, 'System Defaults', '', 'This menu item contains single choice questions that generally require a YES/NO answer or single option selection.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 09:40:39', 'systemDefaults', 0, '/SystemAdmin/index/systemDefaults'),
(2, 'Lookup Tables', '', 'This menu item contains various lookup tables that are used throughout the system to select a single answer from a dropdown.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'lookupTables', 0, '/SystemAdmin/index/lookupTables'),
(3, 'General Setup', '', 'This menu item contains a selection of generalised tables that define the system and the way it works.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'generalSetup', 0, '/SystemAdmin/index/generalSetup'),
(4, 'Organisation Setup', '', 'This menu item contains a selection of tables that define the structure of the organisations that use the system and their relationships.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'organisationSetup', 0, '/SystemAdmin/index/organisationSetup'),
(5, 'Product Setup', '', 'This menu item contains a selection of tables that are used to create and maintain manufacturers and their products.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'productSetup', 0, '/SystemAdmin/index/productSetup'),
(6, 'Financial Setup', '', 'This menu item contains tables and defaults that define the financial structure of the system and various billing processes.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'financialSetup', 0, '/SystemAdmin/index/financialSetup'),
(7, 'Text & Language Setup', '', 'This menu item will allow the user to edit and maintain text that is displayed throughout the system and assign alternative names to selected fields.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'textLanguageSetup', 0, '/SystemAdmin/index/textLanguageSetup'),
(8, 'Alternative Field Names', '', 'This menu item will allow the user to change the field name displayed in Skyline with an alternative that is specific to their organization.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:28', 'alternativeFieldNames', 0, '/SystemAdmin/alternativeFieldNames'),
(9, 'Procedures & Scheduled Tasks', '', 'This menu item contains various automated routines that interact with the system to update selected tables or allocate work and responsibilities.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:29', 'procedures', 0, '/SystemAdmin/index/procedures'),
(10, 'Report Generator', '', 'This menu item will allow the user to access predefined reports or define and run new reports.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:29', 'reportGenerator', 0, '/Report/index'),
(11, 'Job Allocation', '', 'Job Allocation description goes here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:16:29', 'jobAllocation', 0, '/SystemAdmin/index/jobAllocation'),
(12, 'Audit Trail Actions', '', 'This menu item contains a list of actions that will be assigned by the system when an audit trail record is created.   You cannot insert or delete a record, however, you can change its description or make it inactive.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:17:17', 'auditTrailActions', 2, '/LookupTables/auditTrailActions'),
(13, 'Contact History Actions', '', 'This menu item contains a list of Actions that will be assigned to a Contact History Record. You may insert new Actions as required.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'contactHistoryActions', 2, '/LookupTables/contactHistoryActions'),
(14, 'Customer Titles', '', 'This menu item contains a list of Customer titles.  You may add records, however, you should refrain from making this list extensively long.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'customerTitles', 2, '/LookupTables/customerTitles'),
(15, 'Counties', '', 'This menu item contains a list of Counties that can be selected from a drop down when inserting an address.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'county', 2, '/LookupTables/county'),
(16, 'Countries', '', 'This menu item contains a list of Countries that will be used when completing an address and assigning a Country to various system defaults.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'country', 2, '/LookupTables/country'),
(17, 'VAT Rates', '', 'This menu item contains a list of VAT Codes and their associated rates that will be used to calculate VAT on a sales or service item.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'VATRates', 2, '/LookupTables/VATRates'),
(18, 'Job Types', '', 'This menu item contains a list of Types of Job that will define the service required i.e. Installation, Repair, etc.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'jobTypes', 2, '/LookupTables/jobTypes'),
(19, 'Payment Types', '', 'This menu item contains a list of payment types that will be allocated to any money received for a service and will be used in reports to reconcile payments received.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'paymentTypes', 2, '/LookupTables/paymentTypes'),
(20, 'Security Questions', '', 'This menu item contains a list of questions that will prompt the user to remember their password.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'securityQuestions', 2, '/LookupTables/securityQuestions'),
(21, 'Service Types', '', 'This menu item contains a list of items that are related to Job Types and defines what fields are required and displayed on a job booking form.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'serviceTypes', 2, '/LookupTables/serviceTypes'),
(22, 'System Statuses', '', 'This menu item contains a list Statuses that are used by the system to identify where a particular job is currently located during the repair life cycle.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'systemStatuses', 2, '/LookupTables/systemStatuses'),
(23, 'System Status Permissions', '', 'This menu item contains a list of brands, branches, clients, networks, roles or users and the permissions they can assign to a job, if they already have access to this facility.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'systemStatusPermissions', 2, '/LookupTables/systemStatusPermissions'),
(24, 'User Roles', '', 'This menu item contains a list of role’s that are used to describe the job a particular user normally carries out within the business i.e. Branch Manager, Retail Assistant, etc.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'Roles', 2, '/LookupTables/Roles'),
(25, 'RA Status Types', '', 'This menu item will display a list of Statuses used by the Request Authorisation process.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'RAStatusTypes', 2, '/LookupTables/RAStatusTypes'),
(26, 'Couriers', '', 'This menu item contains defaults that will be used to assign couriers and communicate with their automated processes.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'couriers', 2, '/LookupTables/couriers'),
(27, 'Access Permissions', '', 'This menu item contains a list of Access Permissions. It will allow the user to edit the Access Permission name and the Description.  These Access Permission are referenced in the User Roles table.', '0000-00-00 00:00:00', '2013-07-01 01:52:57', 'Active', 1, '2013-07-01 01:58:03', 'accessPermissions', 2, '/LookupTables/accessPermissions'),
(28, 'Accessories', '', 'This menu item contains a list of Accessories that can be assigned to specific unit types and displayed during the booking process.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'accessory', 2, '/LookupTables/accessory'),
(29, 'Colours', '', 'This menu item contains a list of Colours that can be assigned to specific unit types and displayed during the booking process.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'colour', 2, '/LookupTables/colour'),
(30, 'Product Codes', '', 'This menu item contains a list of Product Codes that can be assigned to specific unit types and displayed during the booking process.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'ProductCode', 2, '/LookupTables/ProductCode'),
(31, 'Currency', '', 'This menu item contains a list of currency that can be selected when purchasing from international suppliers. User can maintain exchange rate in this table.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'Currency', 2, '/LookupTables/Currency'),
(32, 'Mobile Phone Network', '', 'This menu option contains a list of Mobile Phone Networks and their country of operation.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'mobilePhoneNetwork', 2, '/LookupTables/mobilePhoneNetwork'),
(33, 'Part Order Status', '', 'This status is used on a part to identify the reason why a part order was raised.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partOrderStatus', 2, '/LookupTables/partOrderStatus'),
(34, 'SMS Messages', '', 'This menu item contains a list of SMS Messages.  It will allow the user to Insert new messages and Edit or Delete existing messages along with the Text that makes up the message.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'sms', 2, '/LookupTables/sms'),
(35, 'Job Fault Codes', '', 'Job Fault Codes are the field names the manufacturer requires when a warranty claim is submitted.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', 1, '2013-07-01 01:16:21', 'jobFaultCodes', 2, '/LookupTables/jobFaultCodes'),
(36, 'Job Fault Code Lookups', '', 'Job Fault Code Lookups are the selectable options that appear when entering job fault codes on a warranty claim.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'jobFaultCodesLookups', 2, '/LookupTables/jobFaultCodesLookups'),
(37, 'Part Fault Codes', '', 'Part Fault Codes are the field names the manufacturer requires when a warranty claim is submitted.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partFaultCodes', 2, '/LookupTables/partFaultCodes'),
(38, 'Part Fault Codes Lookups', '', 'Part Fault Code Lookups are the selectable options that appear when entering part fault codes on a warranty claim.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partFaultCodesLookups', 2, '/LookupTables/partFaultCodesLookups'),
(39, 'Shelf Location', '', 'Shelf locations are the internal storage locations for stock items.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'shelfLocations', 2, '/LookupTables/shelfLocations'),
(40, 'Part Location', '', 'Locations allow users to define where parts are stored internally and allowing multiple site users to configure each site location.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partLocations', 2, '/LookupTables/partLocations'),
(41, 'Repair Types', '', 'Repair Types are used by Service Providers to configure how repairs are managed. For instance, some repair types may require warranty codes or be excluded from invoicing.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'repairTypes', 2, '/LookupTables/repairTypes'),
(42, 'Part Categories', '', 'Part Categories are a means for users to group parts in stock control.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partCategories', 2, '/LookupTables/partCategories'),
(43, 'Part Sub-Categories', '', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partSubCategories', 2, '/LookupTables/partSubCategories'),
(44, 'Part Status Colours', '', 'This menu option will allow a user to define a colour coding against each Part Status.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'partStatusColours', 2, '/LookupTables/partStatusColours'),
(45, 'Group Headings', '', 'This menu item will allow editing of Group Headings and associated descriptive text for all user interface tables within the system.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:18:00', 'groupHeadings', 2, '/LookupTables/groupHeadings'),
(46, 'Access Permissions', '', 'This page will allow the full list of tasks within Skyline which can be controlled by access levels to be entered/viewed.  The list should be a standard jquery browse with the following fields.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:45', 'AccessPermissions', 3, '/generalSetup/AccessPermissions'),
(47, 'General Defaults', '', 'This menu item allows the edititing of the general defaults table.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:45', 'GeneralDefault', 3, '/GeneralSetup/GeneralDefault/'),
(48, 'System Users', '', 'Page description goes here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'systemUsers', 4, '/OrganisationSetup/systemUsers'),
(49, 'Service Networks', '', 'This menu item contains a list of the service networks who are using Skyline.  You can insert or change a record from here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'serviceNetworks', 4, '/OrganisationSetup/serviceNetworks'),
(50, 'Clients', '', 'This menu item contains a list of the clients for each Service Network.   You can insert, change or delete a record from here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'clients', 4, '/OrganisationSetup/clients'),
(51, 'Brands', '', 'This menu item contains a list of the brands for each Client.  You can insert, change or delete a record from here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'brands', 4, '/OrganisationSetup/brands'),
(52, 'Branches', '', 'This menu item contains a list of the branches for each Client.   You can insert, change or delete a record from here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'branches', 4, '/OrganisationSetup/branches'),
(53, 'Service Providers', '', 'This menu item contains a list of Service Providers who are setup on the system.   You can insert, change or delete a record from here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'serviceProviders', 4, '/OrganisationSetup/serviceProviders'),
(54, 'Extended Warrantor', '', 'Description goes here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'extendedWarrantor', 4, '/OrganisationSetup/extendedWarrantor'),
(55, 'Suppliers', '', 'Description goes here.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:21:03', 'suppliers', 4, '/OrganisationSetup/suppliers'),
(56, 'Manufacturers', '', 'Administer manufacturer and assign them to networks.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'manufacturers', 5, '/ProductSetup/manufacturers'),
(57, 'Unit Types', '', 'Add and administer unit types and assign them to clients.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'unitTypes', 5, '/ProductSetup/unitTypes'),
(58, 'Models', '', 'Edit and administer the models database.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'models', 5, '/ProductSetup/models'),
(59, 'Pricing Structure', '', 'Assign a pricing structure for each unit type per client.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'pricingStructure', 5, '/ProductSetup/pricingStructure'),
(60, 'Product Numbers', '', 'Edit client product lists or catalogue.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'productNumbers', 5, '/ProductSetup/productNumbers'),
(61, 'Client Service Types', '', 'Assign Unit Types to Clients.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'clientServiceTypes', 5, '/ProductSetup/clientServiceTypes'),
(62, 'Manufacturer''s Product Groups', '', 'Assign Product Groups for a Manufacturer to Unit Types.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:53', 'unitTypeManufacturer', 5, '/ProductSetup/unitTypeManufacturer'),
(63, 'Central Service Allocations', '', 'This menu item allows products which are repaired centrally to be setup for allocation to one service centre.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:44', 'centralServiceAllocations', 11, '/JobAllocation/centralServiceAllocations'),
(64, 'Postcode Allocations', '', 'This menu item allows service centres to be assigned postcode areas to allow jobs to be automatically assigned.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:44', 'postcodeAllocations', 11, '/JobAllocation/postcodeAllocations'),
(65, 'Town Allocations', '', 'This menu item allows service centres to be assigned a county or town.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:44', 'townAllocations', 11, '/JobAllocation/townAllocations'),
(66, 'Bought Out Guarantee', '', 'This menu item allows manufacturer’s bought out guarantee products to be setup.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:44', 'boughtOutGuarantee', 11, '/JobAllocation/boughtOutGuarantee'),
(67, 'Unallocated Jobs', '', 'This menu item will allow jobs to be assigned to service centres or be cancelled if wrongly assigned.', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Active', NULL, '2013-06-28 11:20:44', 'unallocatedJobs', 11, '/JobAllocation/unallocatedJobs');



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.292');
