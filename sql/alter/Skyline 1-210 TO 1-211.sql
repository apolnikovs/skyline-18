# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.210');

# ---------------------------------------------------------------------- #
# Modify Table appointment	                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE job ADD COLUMN ServiceProviderDespatchDate TIMESTAMP NULL DEFAULT NULL AFTER DateReturnedToCustomer;
				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.211');
