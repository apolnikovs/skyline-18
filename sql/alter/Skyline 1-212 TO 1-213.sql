# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.212');

# ---------------------------------------------------------------------- #
# Modify Table service_provider_engineer	                             #
# ---------------------------------------------------------------------- #
ALTER TABLE service_provider_engineer ADD COLUMN AppsBeforeOptimise TINYINT UNSIGNED NOT NULL DEFAULT '0';

				
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.213');
