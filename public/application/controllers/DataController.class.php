<?php
/**
* An example class, this is grouped with
* other classes in the "sample" package and
* is part of "classes" subpackage
* @package skyline
* @subpackage DataController
*/
require_once('CustomController.class.php');
include_once(APPLICATION_PATH.'/include/phpmailer.class.php');

class DataController extends CustomController {
    
    public $config;
    public $session; 
    public $user;
    public $page;
    public $messages;
    
    public function __construct() { 
        
        parent::__construct();        
        
        /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        $this->session = $this->loadModel('Session');         
        $this->messages = $this->loadModel('Messages');
        
        /*if ($_SERVER['REQUEST_METHOD'] != 'POST') { 
            throw new Exception('Data request is not POST');
        }*/
        
        
        /* ==========================================
         *  Initialise User Class
         * ==========================================
         */
       
        if (isset($this->session->UserID)) {
            
            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
        } 
        
       
       
        
        
                       
    }
       
    public function websiteAction( $args ) {
        
        //$this->log('DataController -> websiteAction');
               
        $model = $this->loadModel('Website');
  
        $function = isset($args[0]) ? $args[0] : '';
        
        switch($function) {
            case 'create':
                $result = $model->create($_POST);
                break;
            case 'update':
                $result = $model->update($_POST);
                break;
            case 'delete':
                $result = $model->delete($_POST);
                break;
            case 'fetch':
                $result = $model->fetch($_POST);
                break;
            default:
                $result = array('status' => 'ERROR',
                                'message' => 'Unrecognised argument');
        }
        
        echo json_encode($result);
  
    }
    
    
    
    public function jobsAction( $args ) {
                            
        $model = $this->loadModel('Job');
  
        $function = isset($args[0]) ? $args[0] : '';
        
        if(method_exists($model, $function)) {
            
            //$this->log('function exists...');
            
            if($function == "search") 
            {
                $_REQUEST['jobSearch'] = isset($args[1]) ? $args[1] : '';
                $_REQUEST['activeFilter'] = isset($args[2]) ? $args[2] : '';
                $_REQUEST['filterID'] = isset($args[3]) ? $args[3] : '';
                $_REQUEST['batchID'] = isset($args[4]) ? $args[4] : '';
                
                $result = $model->$function($_REQUEST);
            }
            else if($function == "serviceProviderOpenJobs" || $function == "serviceProviderAppJobs")
            {
                $_POST['ServiceProviderID'] = isset($args[1]) ? $args[1] : '';
                $_POST['NetworkID']         = isset($args[2]) ? $args[2] : '';
                
                if($function == "serviceProviderAppJobs")
                {
                    $_POST['AppointmentDate']         = isset($args[3]) ? $args[3] : '';
                }
                
                $_POST['aType']         = isset($args['aType']) ? $args['aType'] : false;
                $_POST['sType']         = isset($args['sType']) ? $args['sType'] : false;
                $_POST['dType']         = isset($args['dType']) ? $args['dType'] : false;
                
                
                $result = $model->$function($_POST);
            }    
            
            
            
        } else {
	    
            $result = array('status' => 'ERROR', 'message' => 'Unrecognised argument');
	    
        }
        	
        echo json_encode($result);
  
    }
  
    
    
    public function findServiceCentreAction( $args ) {
       
	//$this->log(var_export("testcase", true));
	//$this->log(var_export($args, true));
        
        $skylineModel = $this->loadModel('Skyline');
        $result       = $skylineModel->findServiceCentre($args);
        
        echo json_encode($result);
        
    }
    
    
    public function permissionAction( $args ) {
        $RoleID       = isset($args[0]) ? $args[0] : '';
        $checkBoxFlag = isset($args[1]) ? $args[1] : false;
        $statusFlag   = isset($args[2]) ? $args[2] : false;
        
        
        $_POST['statusFlag'] = $statusFlag;

        $model = $this->loadModel('Skyline');
        $result = $model->getPermissions($_POST, $RoleID);
        
        
        if($checkBoxFlag)
        {
            $tCnt = count($result['aaData']);
            for($i=0;$i<$tCnt;$i++)
            {
                if($result['aaData'][$i][3])
                {
                    $result['aaData'][$i][3] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$i.'" name="PermissionID_'.$result['aaData'][$i][0].'" value="Tagged" type="checkbox" checked="checked" />';
                }
                else
                {
                   $result['aaData'][$i][3] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$i.'" name="PermissionID_'.$result['aaData'][$i][0].'" type="checkbox" value="Un-tagged" />';
                }
            }
            
        }
        
        echo json_encode($result);
    }
    
    
    public function statusPermissionAction( $args ) {
        $entitytype = isset($args[0]) ? $args[0] : '';
        $id = isset($args[1]) ? $args[1] : '';
        $checkBoxFlag = isset($args[2]) ? $args[2] : false;
        $statusFlag = isset($args[3]) ? $args[3] : false;
        
        $model = $this->loadModel('StatusPermission');
        
        $_POST['statusFlag'] = $statusFlag;

        $result = $model->fetchByEntity($entitytype, $id);
               //$this->log($result);
        if($checkBoxFlag)
        {
            $tCnt = count($result['aaData']);
            for($n=0;$n<$tCnt;$n++)
            {
                if($result['aaData'][$n][2])
                {
                    $result['aaData'][$n][2] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$n.'" name="PermissionID_'.$result['aaData'][$n][0].'" value="Tagged" type="checkbox" checked="checked" />';
                }
                else
                {
                   $result['aaData'][$n][2] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$n.'" name="PermissionID_'.$result['aaData'][$n][0].'" type="checkbox" value="Un-tagged" />';
                }
            } 
        }
        
        echo json_encode($result);
    }
    
     public function OpenJobsStatusPermissionAction( $args ) {
        $entitytype = isset($args[0]) ? $args[0] : '';
        $id = isset($args[1]) ? $args[1] : '';
        $checkBoxFlag = isset($args[2]) ? $args[2] : false;
        $statusFlag = isset($args[3]) ? $args[3] : false;
        
        $model = $this->loadModel('OpenJobsStatusPermission');
        
        $_POST['statusFlag'] = $statusFlag;

        $result = $model->fetchByEntity($entitytype, $id);
               //$this->log($result);
        if($checkBoxFlag)
        {
            $tCnt = count($result['aaData']);
            for($n=0;$n<$tCnt;$n++)
            {
                if($result['aaData'][$n][2])
                {
                    $result['aaData'][$n][2] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$n.'" name="PermissionID_'.$result['aaData'][$n][0].'" value="Tagged" type="checkbox" checked="checked" />';
                }
                else
                {
                   $result['aaData'][$n][2] = '<input class="DTCheckBox" id="PermissionID_RowIndex_'.$n.'" name="PermissionID_'.$result['aaData'][$n][0].'" type="checkbox" value="Un-tagged" />';
                }
            } 
        }
        
        echo json_encode($result);
    }
    
    
    public function getServiceTypesAction( $args ) {
       
        $JobTypeID = isset($args[0]) ? $args[0] : null;
        $NetworkID = isset($args[1]) ? $args[1] : null;
        $BrandID   = isset($args[2]) ? $args[2] : null;
        $ClientID  = isset($args[3]) ? $args[3] : null;

        $model = $this->loadModel('Skyline');
        
        $result  = array();
        if($NetworkID)
        {    
            $result = $model->getServiceTypes($BrandID, $JobTypeID, $NetworkID, $ClientID);
        }
        
         //Comment by Nag.  Joe asked me to do this, users dont see the job booking if service types are not associated with Job type, client.
//        if(count($result)==0)
//        {
//            $result = $model->getServiceTypes(null, $JobTypeID);
//        }
        
        
        echo json_encode($result);
    }
    
      
    
    public function getUserBranchesAction( $args ) {
        
        $UserName = isset($args[0]) ? $args[0] : '';

        
        $model          = $this->loadModel('Skyline');
        $BrandBranchIDs = $model->getUserBrandBranch($UserName);
        
        $result = array();
        
       // $this->log(var_export($BrandBranchIDs, true));
        
        if($BrandBranchIDs)
        {
            $BrandID  = isset($BrandBranchIDs['BrandID'])?$BrandBranchIDs['BrandID']:0;
            $BranchID = isset($BrandBranchIDs['BranchID'])?$BrandBranchIDs['BranchID']:0;
            
           // $this->log(var_export($BrandBranchIDs, true));
            

            if($BrandID && $BranchID && $BrandBranchIDs['BranchType']=='Store' && !$BrandBranchIDs['SuperAdmin'])
            {    
                $result = $model->getBranches($BrandID);
                
                 //$this->log(var_export($BrandBranchIDs, true));
                
                if(is_array($result) && count($result)>0)
                {
                    $result[0]['UserBranchID'] = $BranchID;
                }
            }
        }
        else
        {
            $result[0]['Status'] = "FAIL";
        }
        
        echo json_encode($result);
    }
    
    
    
    
    public function productAction( $args ) {
                    
        $model = $this->loadModel('Product');
  
        $function = isset($args[0]) ? $args[0] : '';
        
        if( method_exists($model, $function) )
        {
            
            if($function=="search")
            {
                $_POST['productSearch'] = isset($args[1]) ? $args[1] : '';
            }
            $result = $model->$function($_POST);
            
            
           // $this->log(var_export($result, true));
            
        }
        else
        {
            $result = array('status' => 'ERROR', 'message' => 'Unrecognised argument');
        }
        
//        switch($function) {
//
//            case 'create':
//                $result = $model->create($_POST);
//                break;
//            case 'delete':
//                $result = $model->delete($_POST);
//                break;
//            case 'fetch':
//                $result = $model->fetch($_POST);
//                break;
//            case 'fetchall':
//                $result = $model->fetchAll($_POST);
//                break;            
//            case 'is_exists':
//                $result = $model->check($_POST);
//                break;              
//            case 'update':
//                $result = $model->update($_POST);
//                break;            
//            default:
//                $result = array('status' => 'ERROR',
//                                'message' => 'Unrecognised argument');
//        }
        
        echo json_encode($result);
  
    }
    
    /**
     * appointmentsAction
     * 
     * @param array $args
     * @return json
     */
    public function appointmentsAction( $args ){
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $skyline_model = $this->loadModel('Skyline');
        
        echo json_encode($skyline_model->getAppointments( $JobID ));        
    }
    
    /*
     * 
     */
    public function brandsAction( $args ){
        
        $name = preg_replace('/\B([A-Z])/', '_$1', $_POST['UserType']);    
        $table    = strtolower($name); 
        
        $skyline = $this->loadModel('Skyline');

        $result = $skyline->getBrands();
        
        #$this->log( 'DataController->brandsAction : ' . var_export($result, true) );
        
        echo json_encode($result);
    }    
    
    
    public function getClientsAction( $args ){
        
        $firstArg = isset($args[0])?$args[0]:'';
        
        $Skyline = $this->loadModel('Skyline');
        $clients = array();
        
        if($firstArg)
        {
            $clients =  $Skyline->getNetworkClients($firstArg);
        }
       
        echo json_encode($clients);
    }
    
    public function getManufacturersAction( $args ){
        
        $firstArg = isset($args[0])?$args[0]:false;
        $secondArg = isset($args[1])?$args[1]:false;
        
        $Skyline = $this->loadModel('Skyline');
       
        $manufacturers = array();
       
        if($secondArg)
        {
            $manufacturers =  $Skyline->getManufacturer('', null, $secondArg);
        }
        
        if(!count($manufacturers) && $firstArg)
        {
            $manufacturers =  $Skyline->getManufacturer('', $firstArg);
        }
        
        if(!count($manufacturers))
        {
             $manufacturers   = $Skyline->getManufacturer();
        }

        echo json_encode($manufacturers);
    }
    
     public function getServiceProvidersAction( $args ){
        
        $firstArg = isset($args[0])?$args[0]:'';
        
        $Skyline = $this->loadModel('Skyline');
       
        $serviceProviders = array();
                
        $serviceProviders =  $Skyline->getNetworkServiceProviders($firstArg);


        echo json_encode($serviceProviders);
    }
    
     public function getServiceProvidersDTListAction( $args ){
        
        $_POST['firstArg']    = isset($args[0])?$args[0]:false;
        $_POST['secondArg']   = isset($args[1])?$args[1]:false;
        $_POST['thirdArg']    = isset($args[2])?$args[2]:false;
        $_POST['fourthArg']   = isset($args[3])?$args[3]:false;
        
        
        $ServiceProviders = $this->loadModel('ServiceProviders');
       
        $serviceProvidersList = array();
                
        $serviceProvidersList =  $ServiceProviders->fetch($_POST);


        echo json_encode($serviceProvidersList);
    }
    
    public function getUsersAction( $args ){
        
        $firstArg  = isset($args[0])?$args[0]:'';
        $secondArg = isset($args[1])?$args[1]:'';
        $thirdArg  = isset($args[2])?$args[2]:'';
        
        $Skyline = $this->loadModel('Skyline');
       
        $usersList = array();
                
        $usersList =  $Skyline->getUsers($firstArg, $secondArg, $thirdArg);


        echo json_encode($usersList);
    }
     
    
    public function getBranchesAction( $args ){
        
        $firstArg  = isset($args[0])?$args[0]:'';
        $secondArg = isset($args[1])?$args[1]:'';
        
        $Skyline = $this->loadModel('Skyline');
               
        $branchesList = array();
        $branchesList =  $Skyline->getBranches(null, null, $firstArg, $secondArg);


        echo json_encode($branchesList);
    }
    
    
    
    public function getOrganisationsAction( $args ){
        
        $firstArg  = isset($args[0])?$args[0]:'';
       
        
        $Skyline = $this->loadModel('Skyline');
               
        
        switch ($firstArg)
        {
           
            case 'Brand':

                 $company_temp =  $Skyline->getBrands();
                 $company = array();
                 foreach($company_temp as $ct)
                 {
                     $company[] = array("ID"=> $ct['BrandID'], "CompanyName"=> $ct['BrandName']);
                 }    
                
            break;
        
            case 'Manufacturer':

                 $company =  $Skyline->getManufacturer('sysuser');
                
            break;
        
            default:
                $company = $Skyline->getServiceProviders(1);
        }
        

        echo json_encode($company);
    }
    
    
    public function getBranchAddressAction( $args ){
        
        $BranchID        =  isset($args[0])?$args[0]:null;
        $ClientID        =  isset($args['ClientID'])?$args['ClientID']:null;
        $NetworkID       =  isset($args['NetworkID'])?$args['NetworkID']:null;
        
        $BranchModel     =  $this->loadModel('Branches');
               
        $BranchDetails   =  $BranchModel->getBranchesWithAddress($ClientID, $NetworkID, $BranchID);
        
        echo json_encode($BranchDetails);
    }
    
    
    public function getStockCodesAction( $args ){
        
        $ClientID        =  isset($args['ClientID'])?$args['ClientID']:null;
        $NetworkID       =  isset($args['NetworkID'])?$args['NetworkID']:null;
        
        $ProductNumbersModel = $this->loadModel('ProductNumbers');
        $stockcode_rows      = $ProductNumbersModel->fetchProductNumbers($NetworkID, $ClientID);
        
        echo json_encode($stockcode_rows);
    }
    
    
     public function getUnitTypesAction( $args ){
        
        $firstArg  = isset($args[0])?$args[0]:'';
        
        $Skyline = $this->loadModel('Skyline');
               
        $unitTypes = array();
                
        $unitTypes =  $Skyline->getUnitTypes(null, $firstArg);

        
        if(!count($unitTypes))
        {
             $unitTypes   = $Skyline->getUnitTypes();
        }
        

        echo json_encode($unitTypes);
    }
    
    
     public function getEngineersAction( $args ){
        
        $firstArg  = isset($args[0])?$args[0]:false;
        $result = array();
        if($firstArg)
        {    
            $EngineersModel = $this->loadModel('Engineers');

            $result =  $EngineersModel->fetchAll($firstArg, 'Active');
        }    

       // $this->log("dddddddddddddddddddddddddddddd");
       // $this->log($result);
        
        echo json_encode($result);
    }
    
    
    
    /*
     * @param array $args
     * http://local.skyline/OrganisationSetup/systemUsers
     */
    public function companyBrandsAction( $args ){

        #$this->log('Skyline->companyBrandsAction :'.var_export($_POST, true));

        $UserType = $_POST['UserType'];
        $CompanyID = (isset($_POST['CompanyID']) && $_POST['CompanyID']!='')? $_POST['CompanyID']: false;
        
        
        $skyline = $this->loadModel('Skyline');
         switch( $UserType ) {

                    case 'ServiceProvider':
                       
                        
                        $company = $skyline->getServiceProviders(1);
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands(false, false, false, false, $CompanyID);
                        }
                        break;

                    case 'Manufacturer':
                       
                        
                        $company = $skyline->getManufacturer("sysuser");
                        
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands(false, false, false, $CompanyID, false);
                        }
                        
                        break;
                    
                    case 'ExtendedWarrantor':
                       
                        
                        $company = $skyline->getExtendedWarrantor("sysuser");
                        
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands();
                        }
                        
                        break;
                    
                    
                    case 'Network':
                        
                        
                        $company = $skyline->getNetworks(1);
                       
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands($CompanyID, false, false, false, false);
                        }
                        
                        break;
                     
                    case 'Client':
                        
                        
                        $company = $skyline->getClients(1);  
                        
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands(false, $CompanyID, false, false, false);
                        }
                        
                        break;
                     
                    case 'Branch':
                        
                        
                        $company = $skyline->getBranches(null, 1); 
                        
                        if($CompanyID)
                        {    
                           $bradsList = $skyline->getBrands(false, false, $CompanyID, false, false);
                        }
                        
                        
                        break;
                    default:
                        $company = array();
                        
         }
        
         
        if(!$CompanyID)
        {    
           $bradsList = $skyline->getBrands();
        }
                        
        
        
        if($_POST['UserType']=='Network')
        {
            $userType = 'Service Network';
        }
        else if($_POST['UserType']=='ServiceProvider')
        {
            $userType = 'Service Provider';
        }
        else if($_POST['UserType']=='ExtendedWarrantor')
        {
            $userType = 'Extended Warrantor';
        }
        else
        {
            $userType = $_POST['UserType'];
        }
        
        
        $userID = ($_POST['UserID']=='' ? null : $_POST['UserID']);
        
        #$this->log('Skyline->collect :'.var_export($access_rights, true));

        echo json_encode(array('company' => $company, 'brand' => $bradsList, 'access_right' => $skyline->getRoles($userID, $userType) ) ); 
    }
    
    
    public function partsUsedAction( $args ){
        
	$JobID = isset($args[0]) ? $args[0] : '';
        $chargeType=  isset($args['chargeType'])?$args['chargeType']:"m";
        $job_model = $this->loadModel('Job');
        
        /*$result = array(
            'sEcho' => 0,
            'iTotalDisplayRecords' => '10',            
        );
        $result['iTotalRecords'] = count($job_model->getPartsUsed($JobID));
        $result['aaData'] = $job_model->getPartsUsed($JobID);*/
        
        $result = $job_model->getPartsUsedDT($JobID,$chargeType);
        
        echo json_encode($result);
    }
    
    
    public function ContactHistoryAction( $args ){
        /*$JobID = isset($args[0]) ? $args[0] : '';
        
        $job_model = $this->loadModel('Job');
        
        $result = array(
            'sEcho' => 0,
            'iTotalDisplayRecords' => '10',            
        );
        $result['iTotalRecords'] = count($job_model->getContactHistory($JobID));
        $result['aaData'] = $job_model->getContactHistory($JobID);
        
        echo json_encode($result);*/ /*-echo '{"sEcho":"0","iTotalDisplayRecords":"1","iTotalRecords":1,"aaData":[{"ContactDate":"2012","ContactTime":"","UserCode":"","Action":"FAXED OUT","Subject":"","Note":"grgerg "}]}';*/
       
        $JobID = isset($args[0]) ? $args[0] : '';
        $contact_history_model = $this->loadModel('ContactHistory');
        
        //echo json_encode($contact_history_model->fetchForJob($args[0]));
        
        $result = $contact_history_model->getContactHistory($_POST, $args[0]);
        
        //$this->log($result);
        
        echo json_encode($result);
    }    
    
    
    
    public function RAhistoryAction($args) {
        
       $args[0] = isset($args[0]) ? $args[0] : 0;  
       $job_model = $this->loadModel("Job");
       if(isset($_GET["type"])) {
	   $_POST["type"] = $_GET["type"];
       }
       $result = $job_model->fetchRaHistory($_POST, $args[0]);
       echo json_encode( $result );
       
    }
    
    
    
    public function ServiceProviderAddressAction($args) {
       
       $args[0] = isset($args[0]) ? $args[0] : 0;
       $skyline_model = $this->loadModel("Skyline");
       $SCAddress = $skyline_model->getServiceCenterDetails($args[0]);
       $SCAddress["FullAddress"] = isset($SCAddress["FullAddress"]) ? $SCAddress["FullAddress"] : "";
       echo json_encode($SCAddress["FullAddress"]);
       
    }
    
    
    
    public function statusChangesAction( $args ){
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $job_model = $this->loadModel('Job');
        
        /*$result = array(
            'sEcho' => 0,
            'iTotalDisplayRecords' => '10',            
        );
        $result['iTotalRecords'] = count($job_model->getStatusChanges($JobID));
        $result['aaData'] = $job_model->getStatusChanges($JobID);*/
        
        $result = $job_model->getStatusChangesDT($JobID);
        
        #$this->log( var_export($result, true));
        
        echo json_encode($result);
        
    }

    
    
    public function updateAction($args) {
        
	$dataset = isset($args[0]) ? $args[0] : '';
        
        //$this->log('updateAction' . var_export($_POST, true));
        
        if(isset($_POST['StockCode'])) {
            $result = ['message_type' => 'error', 'message' => ' Stock Code do not match with database', 'field' => 'StockCode'];
        } else {
            $result = ['message_type' => 'info', 'message' => $dataset . ' data sent to server but not added to database'];            
        }
        
        echo json_encode($result);
    }

    
    
    public function updateAdditionalInformationAction($args) {
        
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $jobModel = $this->loadModel('Job'); 
	
        $fields['JobID'] = $JobID;
        $fields['Insurer'] = $_POST['Insurer'];
        $fields['PolicyNo'] = $_POST['PolicyNo'];
        if (isset($_POST['AuthorisationNo'])) $fields['AuthorisationNo'] = $_POST['AuthorisationNo'];
        $fields['AgentRefNo'] = $_POST['BrandReferralNo'];
  
	$job = $jobModel->fetch($JobID);
	if(isset($_POST['AuthorisationNo']) && ($job["AuthorisationNo"] == null || $job["AuthorisationNo"] == "") && $job["AuthorisationNo"] != $_POST['AuthorisationNo']) {
	    $processAccepted = true;
	} else {
	    $processAccepted = false;
	}

	
	/* save a copy of job prior to updating any fields */
     $sb_model = $this->loadModel('ServiceBaseBusinessModel');
     $sb_model->FetchSkylineJob($JobID);

	$courierModel = $this->loadModel("Couriers");
	$courierModel->updateJobShippingData($_POST, $JobID);
	
        $mdl_result = $jobModel->update($fields);

    /* send updated fields to ServiceBase */
    $sb_model->PutJobDetails($JobID);
        
	if($processAccepted) {
	    $jobModel->processRAAccepted($JobID);
	} else {
	    $api = $this->loadModel('APIJobs');
	    $api->rmaPutJobDetails($JobID);
	}
	
        if($mdl_result['status'] == 'SUCCESS') {
            $result = ['message_type' => 'info', 'message' => $JobID . ' updated OK', 'data' => []];
        } else {
            $result = ['message_type' => 'error', 'message' => $mdl_result['message'], 'field' => ''];
        }
        
        echo json_encode($result);
    }
    
    
    
    public function updateAdditionalNotesAction($args){
               
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $job_model = $this->loadModel('Job'); 
        
        $fields['JobID'] = $JobID;
        $fields['Notes'] = $_POST['Notes'];
  
        /* save a copy of job prior to updating any fields */
        $sb_model = $this->loadModel('ServiceBaseBusinessModel');
        $sb_model->FetchSkylineJob($JobID);

        $mdl_result = $job_model->update($fields);

        /* send updated fields to ServiceBase */
        $sb_model->PutJobDetails($JobID);

        $api = $this->loadModel('APIJobs');
        $api->rmaPutJobDetails($JobID);        

        if($mdl_result['status'] == 'SUCCESS') {
            $result = ['message_type' => 'info', 'message' => $JobID . ' updated OK', 'data' => []];
        } else {
            $result = ['message_type' => 'error', 'message' => $mdl_result['message'], 'field' => ''];
        }
        
        echo json_encode($result);
    }

    
    
    public function updateProductDetailsAction($args) {
        
        $JobID = isset($args[0]) ? $args[0] : "";

	$clientID = isset($args[1]) ? $args[1] : null;
	
        if(!isset($_POST['ProductNo']) || $_POST['ProductNo'] == null) {
            $product = false;
        } else {
            $product_model = $this->loadModel('Product');       
            $product = $product_model->select($_POST['ProductNo'], $clientID);
        }
        
        $job_model = $this->loadModel('Job');

        $job_fields['JobID'] = $JobID;
        
        if($product !== false) {
            $job_fields['ProductID'] = $product['ProductID'];
            $job_fields['ManufacturerID'] = $product['ManufacturerID'];
            $job_fields['ModelID'] = $product['ModelID'];
        } else {
            //SSSS this was causing Catalogue Number to be removed? Why?
    	    //$job_fields['ProductID'] = null;
	    $job_fields['ProductID'] = $product['ProductID'];
            $job_fields['ManufacturerID'] = ($_POST['ManufacturerID'] == '') ? null : $_POST['ManufacturerID'];
            $job_fields['ModelID'] = ($_POST['ModelID'] == '') ? null : $_POST['ModelID'];
        }
        
	
        if(isset($_POST["ServiceTypeEditable"]) && $_POST["ServiceType"] != '-1') {    
            $job_fields["ServiceTypeID"] = ($_POST["ServiceType"] == '') ? null : $_POST["ServiceType"];
        }
        
        $job_fields["ImeiNo"] = isset($_POST["ImeiNo"]) ? $_POST["ImeiNo"] : null;
	
        $job_fields['ServiceBaseManufacturer'] = $_POST['ServiceBaseManufacturer'];
        $job_fields['ServiceBaseModel'] = $_POST['ServiceBaseModel'];
        $job_fields['ServiceBaseUnitType'] = $_POST['ServiceBaseUnitType'];
        
        $job_fields['ProductLocation'] = ($_POST['PhysicalLocation'] == '') ? null : $_POST['PhysicalLocation'];
        if($_POST['RepairType'] != '') $job_fields['RepairType'] = $_POST['RepairType'];
        $job_fields['SerialNo'] = $_POST['SerialNo'];
        $job_fields['OriginalRetailer'] = $_POST['OriginalRetailer'];
        $job_fields['ReceiptNo'] = $_POST['ReceiptNo'];
        if($_POST['PurchaseDate_1'] != '') {
            $dateArray = explode("/", $_POST['PurchaseDate_1']);
            if(count($dateArray) == 3) {    
		$job_fields['DateOfPurchase'] = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
            }
        }
        
        /* save a copy of job prior to updating any fields */
        $sb_model = $this->loadModel('ServiceBaseBusinessModel');
        $sb_model->FetchSkylineJob($JobID);

        $jmdl_result = $job_model->update($job_fields);

        /* send updated fields to ServiceBase */
        $sb_model->PutJobDetails($JobID);

        $api = $this->loadModel('APIJobs');
        $api->rmaPutJobDetails($JobID);
            
        if($jmdl_result['status'] == 'SUCCESS') {
                       
            $data = [
                //'ProductID' => $job_fields['ProductID'],
                'ManufacturerID' => $job_fields['ManufacturerID'],
                'ModelID' => $job_fields['ModelID']               
            ];
            if($product !== false) {
                $data['ManufacturerName'] = $product['ManufacturerName'];
                $data['UnitTypeID'] = $product['UnitTypeID'];
                $data['ModelNumber'] = $product['ModelNumber'];
                //$data['ModelDescription'] = $product['ModelDescription'];
                $data['UnitTypeName'] = $product['UnitTypeName'];
            } else {
                $data['ManufacturerName'] = $job_fields['ServiceBaseManufacturer'];
                $data['UnitTypeID'] = $_POST['UnitTypeID'];
                $data['ModelNumber'] = $_POST['ModelNumber'];
                //$data['ModelDescription'] = $_POST['ModelDescription'];
                $data['UnitTypeName'] = $_POST['ServiceBaseUnitType'];
            }
            $result = ['status' => 'info', 'message' => $JobID . ' updated OK', 'data' => $data];
            
        } else {
	    
            $result = ['status' => 'error', 'message' => $jmdl_result['message'], 'field' => ''];
	    
        }  
        
        echo json_encode($result);
    }

    
    
    /*
     * @param array $args( $action, $roleID )
     */
    public function userRolesAction( $args ){
        $action = isset($args[0]) ? $args[0] : '';
        
        $args[1] = isset($args[1])?$args[1]:'';

        $user_role = $this->loadModel('UserRoles');
        $skyline = $this->loadModel('Skyline');
        
        switch($action){
            case 'fetch':
                $data = $skyline->getRoles($args[1]);
                break;
            case 'insert';
                $data = $user_role->createUserRole(array(
                    'UserID' => $args[1],
                    'RoleID' => $_POST['RoleID']
                    ));                
                break;
            case 'delete';
                $data = $user_role->deleteUserRole(array(
                    'UserID' => $args[1],
                    'RoleID' => $_POST['RoleID']
                    ));                
                break;            
            default:
                throw new Exception('missing user data controller user roles action');
        }

        #$this->log('Data->userRolesAction :'.var_export($_POST, true));

        echo json_encode($data);
        
    }    

    public function updateIssueReportAction( $args ){
        
        $JobID = isset($args[0]) ? $args[0] : '';
                
        $fields['JobID'] = $JobID;
        $fields['ReportedFault'] = $_POST['IssueReport'];
		
        /* save a copy of job prior to updating any fields */
        $sb_model = $this->loadModel('ServiceBaseBusinessModel');
        $sb_model->FetchSkylineJob($JobID);
            
        $job_model = $this->loadModel('Job');            
        $mdl_result = $job_model->update($fields);
        
        /* send updated fields to ServiceBase */
        $sb_model->PutJobDetails($JobID);

        $api = $this->loadModel('APIJobs');
        $api->rmaPutJobDetails($JobID);
            
        if ( $mdl_result['status'] == 'SUCCESS' ) {
            $result = array('message_type' => 'info', 'message' => $JobID . ' updated OK', 'data' => array());
        } else {
            $result = array('message_type' => 'error', 'message' => $mdl_result['message'], 'field' => '');
        }      
        
        echo json_encode($result);
    }
    
    
    
    
    /**
     * updateUserStatusPreferences
     * 
     * This method is used for to update user preferences status (RA Status or Job statuses)
     * 
     * 
     * @param   array $args     Passed arguments
     * @return  void
     * 
     * @author Nageswara Rao <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    
     public function updateUserStatusPreferencesAction( $args ){
        
        $Page     = isset($args['Page']) ? $args['Page'] : false;
        $Type     = isset($args['Type']) ? $args['Type'] : false;
        $StatusID = (isset($_POST['StatusID']) && is_array($_POST['StatusID'])) ? $_POST['StatusID'] : array();
        
        
        
            
        $SystemStatusesModel = $this->loadModel('SystemStatuses');            
        $SystemStatusesModel->updateUserStatusPreferences($Page, $Type, $StatusID);
        
       
        
    }
    
    
    
    
    
     /**
     * checkManufacturerWanrranty
     * 
     * This method is used for to check Manufacturer Wanrranty
     * 
     * 
     * @return  void
     * 
     * @author Nageswara Rao <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    
     public function checkManufacturerWanrrantyAction() {
        
        $result = false;
        
        $DateOfPurchase = isset($_POST['DateOfPurchase'])?$_POST['DateOfPurchase']:false;
        $ServiceTypeID  = isset($_POST['ServiceTypeID'])?$_POST['ServiceTypeID']:false;
        $UnitTypeID     = isset($_POST['UnitTypeID'])?$_POST['UnitTypeID']:false;
        
        
            
        if($DateOfPurchase && $ServiceTypeID && $UnitTypeID)
        {   
            $dateArray          = explode('/', $DateOfPurchase);
            $DateOfPurchase     = $dateArray[2].'-'.$dateArray[1].'-'.$dateArray[0];
            
            $ServiceTypesModel  = $this->loadModel('ServiceTypes');            
            $ServiceTypeDetails = $ServiceTypesModel->fetchRow(array('ServiceTypeID'=>$ServiceTypeID));
            
            if($ServiceTypeDetails['Code']=='W')
            {
                $UnitTypesModel  = $this->loadModel('UnitTypes');
                $UnitTypeDetails =  $UnitTypesModel->fetchRow(array('UnitTypeID'=>$UnitTypeID));
                
                if($UnitTypeDetails['DOPWarrantyPeriod'])
                {
                    $d1 = new DateTime($DateOfPurchase);
                    $d2 = new DateTime();

                    $interval = date_diff($d1, $d2);
                    $months =  $interval->m + ($interval->y * 12);

                    if($months>$UnitTypeDetails['DOPWarrantyPeriod'])
                    {
                        $result = true;    
                    }
                }    
            }    
        }
        
        
        echo json_encode($result); 
        
    }
    
    
    
    /**
     * isViamenteKeyExists
     * 
     * This method is used for to check ViamenteKey Exists or Not
     * 
     * 
     
     * @return  void
     * 
     * @author Nageswara Rao <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    
     public function isViamenteKeyExistsAction() {
        
        $result = array(false, false);
        
        $ViamenteKey = isset($_POST['ViamenteKey'])?$_POST['ViamenteKey']:false;
        $ViamenteKey2 = isset($_POST['ViamenteKey2'])?$_POST['ViamenteKey2']:false;
        $ExceptServiceProviderID = isset($_POST['ServiceProviderID'])?$_POST['ServiceProviderID']:false;
            
        if($ViamenteKey && $ExceptServiceProviderID)
        {   
            $ServiceProvidersModel = $this->loadModel('ServiceProviders');            
            $key1 = $ServiceProvidersModel->isViamenteKeyExists($ViamenteKey, $ExceptServiceProviderID);
            
            if($key1)
            {
                $result[0] = true;
            }    
        }
        
        if($ViamenteKey2 && $ExceptServiceProviderID)
        {   
            $ServiceProvidersModel = $this->loadModel('ServiceProviders');            
            $key2 = $ServiceProvidersModel->isViamenteKeyExists($ViamenteKey2, $ExceptServiceProviderID);
            
            if($key2)
            {
                $result[1] = true;
            }    
        }
        
        echo json_encode($result); 
        
    }
    
    
    
    
    
    
    
    
     /**
     * updatePreferentialClientsManufacturers
     * 
     * This method is used for to update Preferred Client Group
     * 
     * 
     * @param   array $args     Passed arguments
     * @return  void
     * 
     * @author Nageswara Rao <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
    
     public function updatePreferentialClientsManufacturersAction( $args ){
        
        $Page     = isset($args['Page']) ? $args['Page'] : false;
        
        
        $UsersModel = $this->loadModel('Users');            
        $UsersModel->updatePreferentialClientsManufacturers($Page, $_POST);
        
    }
    
    
    /**
     * updateUserGaugePreferences
     * 
     * Update the user's preferences for the dials on the Graphical Analysis 
     * pages.
     * 
     * @param   array $args     Passed arguments
     * @return  void
     * 
     * @author Andrew J. Williams <Andrew.Williams@awcomputech.com>  
     **************************************************************************/
    
     public function updateUserGaugePreferencesAction( $args ){
        $users_model = $this->loadModel('Users');            
        
       //$this->log($_POST);
       
       $fields = $_POST;
       $fields['UserID'] = $this->user->UserID;
       
       $users_model->update_gauge_preferences($fields);
        
    }
    
    
     /**
     * getRepairSkillSkillsSet
     * 
     * This method is used to get the skills set for given reapir skill and service proivder id.
     * 
     * 
     * @param   array $args     Passed arguments
     * @return  Result JSON encoded
     * 
     * @author Nageswara Rao <nag.phpdeveloper@gmail.com>  
     **************************************************************************/
     public function getRepairSkillSkillsSetAction( $args ){
        
        $RepairSkillID     = isset($args[0]) ? $args[0] : false;
        $ServiceProviderID = isset($args[1]) ? $args[1] : false;
                
        
        
        $ServiceProviderSkillsSet_Model = $this->loadModel('ServiceProviderSkillsSet');
        $result = $ServiceProviderSkillsSet_Model->getRepairSkillSkillsSet($RepairSkillID, $ServiceProviderID);
            
            
        
        echo json_encode($result);
    } 
    
    
    
    
     /**
     * isViamenteEndDayDateExists
     * 
     * This method is used for to check Viamente End Day Date exists or not for given $ServiceProviderID and Date
     *
     * @param array $args
     
     *    
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/
     public function isViamenteEndDayDateExistsAction( $args ){
        
        
        $ServiceProviderID = isset($args[0]) ? $args[0] : false;
        $Date     = isset($_POST['ViamenteConsolidation']) ? $_POST['ViamenteConsolidation'] : false;
                
        
        
        $ServiceProviderSkillsSet_Model = $this->loadModel('ServiceProviderSkillsSet');
        $result = $ServiceProviderSkillsSet_Model->isViamenteEndDayDateExists($ServiceProviderID, $Date);
            
            
        
        echo json_encode($result);
    }
    
    
    
    /**
     * getModelNumbers
     * 
     * This method is used for to get Model Number details.
     *
     * @param array $args
     
     *    
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/
     public function getModelNumbersAction( $args ){
        
        $ModelsModel          = $this->loadModel('Models');
        
        if(isset($args['list']) && $args['list'])
        {    
            if(isset($args['manufacturer']) && $args['manufacturer']!='')
            {
                
            }   
            else
            {
                $args['manufacturer'] = false;
            }
            
            
            $model_no_rows        = $ModelsModel->fetchModelNumbers("ModelNumber", $_REQUEST['name_startsWith'], false, $args['manufacturer']);
            $model_no_rows_count  = count($model_no_rows);

            $result['totalResultsCount'] = $model_no_rows_count;
            $result['models'] = $model_no_rows;

            
        }
        else
        {
            $model_no_rows    = $ModelsModel->fetchModelNumbers("ModelDescription, UnitTypeID, ManufacturerID, ModelID", false, $_REQUEST['ModelNumber'], $args['manufacturer']);
            $result           = isset($model_no_rows[0])?$model_no_rows[0]:false;   
        }
        
        echo json_encode($result);
    }
    
    
    
    
    /**
     * getRetailerNames
     * 
     * This method is used for to get Retailer Names.
     *
     * @param array $args
     
     *    
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/
     public function getRetailerNamesAction( $args ){
        
        $RetailersModel          = $this->loadModel('Retailers');
        
          
        if(isset($args['BranchID']) && $args['BranchID']!='')
        {

        }   
        else
        {
            $args['BranchID'] = false;
        }


        $retailers_rows        = $RetailersModel->fetchAll($args['BranchID'], "RetailerName", $_REQUEST['name_startsWith']);
        
        $result['totalResultsCount'] =  count($retailers_rows);
        $result['retailers']            =  $retailers_rows;

        
        echo json_encode($result);
    }
    
    
    
    
    
    /**
     * saveOriginalRetailer
     * 
     * This method is used for to save Retailer Name.
     *
     * @param array $args
     
     *    
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/
     public function saveOriginalRetailerAction( $args ){
        
        $result = array();
         
        $RetailersModel          = $this->loadModel('Retailers');
        
          
        if(isset($args['BranchID']) && $args['BranchID']!='')
        {

        }   
        else
        {
            $args['BranchID'] = false;
        }

        if(isset($_POST['RetailerName']) && $_POST['RetailerName'])
        {
            $retailerDetails  = $RetailersModel->fetchRow(array('BranchID'=>$args['BranchID'], "RetailerName"=>$_POST['RetailerName']));
            
            if(isset($retailerDetails['RetailerID']))
            {    
                $dataArray = array('RetailerID'=>$retailerDetails['RetailerID'], 'RetailerName'=>$_POST['RetailerName'], 'BranchID'=>$args['BranchID'], 'Status'=>'In-active');
                $result = $RetailersModel->processData($dataArray);
            }
        }
       
        echo json_encode($result);
    }
    
    
    
    
    
    
    
    /**
     * updateInvoiceCosts
     * 
     * Update the invoice costs.
     * TODO: Invoicing in a later version
     * 
     * @param array $args         Passed arguments
     * @return  Result JSON encoded
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function updateInvoiceCostsAction( $args ) {
        
        if (isset($this->session->UserID)) {

            $jobID = isset($args[0]) ? $args[0] : '';

            $job_model = $this->loadModel('Job');
            
            $fields['JobID'] = $jobID;
            
            $wrongField = '';
            $CalculateCost = true;
            if(!is_numeric($_POST['Parts']))
            {
                $CalculateCost = false;
                $wrongField    = 'Parts';
            } 
            
            if($CalculateCost && !is_numeric($_POST['Labour']))
            {
                $CalculateCost = false;
                $wrongField    = 'Labour';
            }
            
            if($CalculateCost && !is_numeric($_POST['Carriage']))
            {
                $CalculateCost = false;
                $wrongField    = 'Carriage';
            }    
            
            if($CalculateCost)
            {
                if (strpos($_POST['Parts'], '.') === false) {
                    
                    if(strlen($_POST['Parts'])>1)
                    {
                        $_POST['Parts'] = substr_replace($_POST['Parts'], '.', -2, 0); 
                    }
                    else if(strlen($_POST['Parts'])==1)
                    {
                        $_POST['Parts'] = '0.0'.$_POST['Parts'];
                    }
                    else
                    {
                        $_POST['Parts'] = '0.00';
                    }
                    
                    
                }
                if (strpos($_POST['Labour'], '.') === false) {
                    
                    
                    if(strlen($_POST['Labour'])>1)
                    {
                        $_POST['Labour'] = substr_replace($_POST['Labour'], '.', -2, 0); 
                    }
                    else if(strlen($_POST['Labour'])==1)
                    {
                        $_POST['Labour'] = '0.0'.$_POST['Labour'];
                    }
                    else
                    {
                        $_POST['Labour'] = '0.00';
                    }
                    
                    
                }
                if (strpos($_POST['Carriage'], '.') === false) {
                    
                    if(strlen($_POST['Carriage'])>1)
                    {
                        $_POST['Carriage'] = substr_replace($_POST['Carriage'], '.', -2, 0); 
                    }
                    else if(strlen($_POST['Carriage'])==1)
                    {
                        $_POST['Carriage'] = '0.0'.$_POST['Carriage'];
                    }
                    else
                    {
                        $_POST['Carriage'] = '0.00';
                    }
                    
                    
                    
                }
                
                $fields['ChargeablePartsCost'] = (float)$_POST['Parts'];
                $fields['ChargeableLabourCost'] = (float)$_POST['Labour'];
                $fields['ChargeableDeliveryCost'] = (float)$_POST['Carriage'];

                
                $this->log('ChargeablePartsCost: '.$fields['ChargeablePartsCost']);
                $this->log('ChargeableLabourCost: '.$fields['ChargeableLabourCost']);
                $this->log('ChargeableDeliveryCost: '.$fields['ChargeableDeliveryCost']);
                
                
    //            $fields['ChargeableSubTotal'] = $_POST['TotalNet'];
    //            $fields['ChargeableVATCost'] = $_POST['VAT'];
    //            $fields['ChargeableTotalCost'] = $_POST['Gross'];

                
                $VATRatesModel = $this->loadModel('VATRates');
                $vatRateReult = $VATRatesModel->fetchVatRate('T1');
                
                
                
                $fields['ChargeableSubTotal']  = $_POST['Parts']+$_POST['Labour']+$_POST['Carriage'];
                $fields['ChargeableVATCost']   = (($fields['ChargeableSubTotal']*$vatRateReult)/100);
                $fields['ChargeableTotalCost'] = $fields['ChargeableSubTotal']+$fields['ChargeableVATCost'];

                /* save a copy of job prior to updating any fields */
                $sb_model = $this->loadModel('ServiceBaseBusinessModel');
                $sb_model->FetchSkylineJob($jobID);

                $mdl_result = $job_model->update($fields);

                /* send updated fields to ServiceBase */
                $sb_model->PutJobDetails($jobID);

                $api = $this->loadModel('APIJobs');
                $api->rmaPutJobDetails($jobID);

                if ( $mdl_result['status'] == 'SUCCESS' ) {
                    $result = array('message_type' => 'info', 'message' => $jobID . ' updated OK', 'data' => array());
                } else {
                    $result = array('message_type' => 'error', 'message' => $mdl_result['message'], 'field' => '');
                }
            
             }
             else
             {
                 $result = array('status' => 'error', 'message' => 'Please enter valid input for each field.', 'field' => $wrongField);
             }
        
        } else {
            $result = array('Status' => 'ERROR', 'Message' => "Please login to update the custormer details.");
        } 
         
        echo json_encode($result);
    }
    
    /**
     * 
     * updateCustomerDetailsAction Fn
     * @param string $args URL segment
     * @return json
     * @todo update the audit action
     * 
     ********************************************/
    public function updateCustomerDetailsAction( $args )
    {
        
         if (isset($this->session->UserID)) 
         {
             
            $CustomerID = isset($args[0]) ? $args[0] : '';
            $JobID = isset($args[1]) ? $args[1] : '';
			
            /* save a copy of job prior to updating any fields */
            $sb_model = $this->loadModel('ServiceBaseBusinessModel');
            $sb_model->FetchSkylineJob($JobID);
        
            $customer = $this->loadModel('Customer');

            $customer->updateByID( $_POST, $CustomerID );

            #$this->log( var_export($customer->updateByID( $_POST, $CustomerID ), true) );
            
            /* send updated fields to ServiceBase */
            $sb_model->PutJobDetails($JobID);

            $api = $this->loadModel('APIJobs');
            $api->rmaPutJobDetails($JobID);

            $result = array('Status' => 'OK', 'Message' => "Customer: {$CustomerID} data sent to server and added to database");

            
         }
         else
         {
             $result = array('Status' => 'ERROR', 'Message' => "Please login to update the custormer details.");
         }  
         
         echo json_encode($result);
    } 

    
    
    public function updateDeliveryDetailsAction($args) {
        
	if (isset($this->session->UserID)) {

	   $CustomerID = isset($args[0]) ? $args[0] : '';
	   $jobID = isset($args[1]) ? $args[1] : '';
	   
	    /* save a copy of job prior to updating any fields */
         $sb_model = $this->loadModel('ServiceBaseBusinessModel');
         $sb_model->FetchSkylineJob($jobID);

	   $job_model = $this->loadModel("Job");
	   $result = $job_model->updateDeliveryDetails($_POST,$jobID);
	   
        /* send updated fields to ServiceBase */
        $sb_model->PutJobDetails($jobID);

	   #$this->log( var_export($customer->updateByID( $_POST, $CustomerID ), true) );

	   $api = $this->loadModel('APIJobs');
	   $api->rmaPutJobDetails($jobID);

	   $result = array('Status' => 'OK', 'Message' => "Job {$jobID} data sent to server and added to database");

	} else {

	    $result = array('Status' => 'ERROR', 'Message' => "Please login to update the job details.");

	}
	
	//$this->log("Update Delivery: " . $result);

	echo json_encode($result);
	 
    } 
    
    
    
    
    /**
     * getEngineerAppointments
     * 
     * This method is used for to check engineer appointments
     *
     * 
     *    
     * @return void 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     **************************************************************************/
    public function getEngineerAppointmentsAction() {
        
	if (isset($this->session->UserID)) {

            $args = $_POST;
            
            $weekArray = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            
            $start_date_time = strtotime($args['start_date']);
            
            $appointment_model = $this->loadModel("Appointment");
            $engineers_model   = $this->loadModel("Engineers");
            
	   
            $ServiceProviderEngineerID = $args['ServiceProviderEngineerID'];
            
            
            $messages = array(); 
            
            $cur_date = date('Y-m-d');
            
            for($i=1;$i<=4;$i++)
            {
                  $j=0;
                  foreach($weekArray as $week)
                  {
                       
                        if(!isset($args[$week.'Active'.$i]))
                        {
                            
                             $WorkDate  = date("Y-m-d", strtotime("+".((($i-1)*7)+$j)." day", $start_date_time));

                             if($WorkDate>=$cur_date)//Rob requested this in Trackerbase log:246
                             {
                                $resultAppointments = $appointment_model->getAppointment($ServiceProviderEngineerID, $WorkDate);
                            
                                foreach ($resultAppointments AS $result)
                                {    
                                      if(isset($result['AppointmentID']) && $result['AppointmentID']!='')
                                      {
                                          $engineers =  array();

                                          if($result['ForceEngineerToViamente'])
                                          {    
                                              $data_args      = array('ServiceProviderSkillsetID'=>$result['ServiceProviderSkillsetID'], 'WorkDate'=>$WorkDate, 'Status'=>'Active', 'ServiceProviderID'=>$result['ServiceProviderID'], 'ServiceProviderEngineerID'=>$result['ServiceProviderEngineerID']);
                                              $engineers_temp = $engineers_model->getEligibleEngineers($data_args);

                                              $engineers[0] = array("ServiceProviderEngineerID"=>0, "Name"=>'Unspecified (allow Viamente to select engineer)');

                                              for($e=0;$e<count($engineers_temp);$e++)
                                              {
                                                  $engineers[$e+1]   = $engineers_temp[$e];
                                              }
                                          }    


                                          $result['EngCount']  = count($engineers);
                                          $result['Engineers'] = $engineers;

                                          $messages[] = $result;
                                      } 
                                }
                              
                             }
                        }

                        $j++;        
                  }

            }
            
           
          

	   $result = array('Status' => 'OK', 'Count' => count($messages), 'Message' => $messages);

	} else {

	    $result = array('Status' => 'ERROR', 'Count'=>1, 'Message' => "Please login to do this action.");

	}
	
       // $this->log($result);
        

	echo json_encode($result);
	 
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * updateServiceReportAction
     * 
     * Update the RepairDescription field in a job.
     * 
     * @param array $args         Passed arguments
     * @return  Result JSON encoded
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function updateServiceReportAction( $args ) {
        
        $JobID = isset($args[0]) ? $args[0] : '';
        
        $fields['JobID'] = $JobID;
        $fields['RepairDescription'] = $_POST['ServiceReport'];
		
        /* save a copy of job prior to updating any fields */
        $sb_model = $this->loadModel('ServiceBaseBusinessModel');
        $sb_model->FetchSkylineJob($JobID);

        $job_model = $this->loadModel('Job'); 
        $mdl_result = $job_model->update($fields);
		
        /* send updated fields to ServiceBase */
        $sb_model->PutJobDetails($JobID);
        
        $api = $this->loadModel('APIJobs');
        $api->rmaPutJobDetails($JobID);        

        if ( $mdl_result['status'] == 'SUCCESS' ) {
            $result = array('message_type' => 'info', 'message' => $JobID . ' updated OK', 'data' => array());
        } else {
            $result = array('message_type' => 'error', 'message' => $mdl_result['message'], 'field' => '');
        }      
       
        echo json_encode($result);
    }
    
     public function getBrandNameAction( $args ) {
                
        $result = false; 
        $Skyline = $this->loadModel('Skyline');
  
        $BrandID = isset($args[0]) ? $args[0] : '';
        
        if($BrandID!='')
        {
            $BrandDetails  = $Skyline->getBrand($BrandID);

            if(isset($BrandDetails[0]['Name']) && $BrandDetails[0]['Name']!='' )
            {
                 $result = $BrandDetails[0]['Name'];
            }    
        }
        
  
        echo json_encode($result);
    }
    
    
    
    public function getCountryNameAction( $args ) {
                
        $result = false; 
        $Skyline = $this->loadModel('Skyline');
  
        $CountryID = isset($args[0]) ? $args[0] : '';
        
        if($CountryID!='')
        {
            $result  = $Skyline->getCountryName($CountryID);
    
        }
        
        echo json_encode($result);
    }
    
    
    
     public function getLatestRAHisotryStatusIDAction( $args ) {
                
        $result = 0; 
        $RAStatusTypes = $this->loadModel('RAStatusTypes');
  
        $JobID  = isset($args[0]) ? $args[0] : false;
        $RAType = isset($args[1]) ? $args[1] : false;
        
        if($JobID)
        {
            $result  = $RAStatusTypes->getLatestRAHisotryStatusID($JobID, $RAType);
        }
        
        echo json_encode($result);
    }
    
    
    
    
     public function getCountryAction( $args ) {
                
        $result = array(); 
        $Skyline = $this->loadModel('Skyline');
  
        $CountryID = isset($args[0]) ? $args[0] : '';
        
        if($CountryID!='')
        {
            $name  = $Skyline->getCountryName($CountryID);
    
            if($name)
            {    
                $result = array(0=>array("CountryID"=>$CountryID, "Name"=>$name));
            }
        }
        
        echo json_encode($result);
    }
    
     public function getDiaryAllocationAction( /* $args */ ) {
                
        $result = false; 
       
        $ServiceProviderEngineerID   = isset($_POST['ServiceProviderEngineerID']) ? $_POST['ServiceProviderEngineerID'] : false;
        $AllocatedDate               = isset($_POST['AllocatedDate']) ? $_POST['AllocatedDate'] : false;
        $AllocatedToDate             = isset($_POST['AllocatedToDate']) ? $_POST['AllocatedToDate'] : false;
        $AppointmentAllocationSlotID = isset($_POST['AppointmentAllocationSlotID']) ? $_POST['AppointmentAllocationSlotID'] : false;
        
        if($ServiceProviderEngineerID && $AllocatedDate && $AllocatedToDate && $AppointmentAllocationSlotID)
        {
            $ServiceProviderSkillsSet = $this->loadModel('ServiceProviderSkillsSet');
            
            $result  = $ServiceProviderSkillsSet->getDiaryAllocation($ServiceProviderEngineerID, $AllocatedDate, $AllocatedToDate, $AppointmentAllocationSlotID);
       
        }
       
       // $this->log("test:");
      //  $this->log(var_export($result, true));
        
        echo json_encode($result);
    }

    
    /**
     * StatusChangeReportAction
     * 
     * Generate the status change reports and e-mail.
     * 
     * @param array 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function StatusChangeReportAction() {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        
        ini_set('memory_limit', '8129M');                                       /* Increase memeory availiable to this method */
        ini_set('max_execution_time', 300);                                     /* Increase execution time for this method */
                
        $appointment_model = $this->loadModel('Appointment');
        $contact_history_model = $this->loadModel('ContactHistory');
        $job_model = $this->loadModel('Job');
        $status_history_model = $this->loadModel('StatusHistory');
        
        $statuses = array(                                                      /* Order of statuses */
                          '01 UNALLOCATED JOB',
                          '02 ALLOCATED TO AGENT',
                          '03 TRANSMITTED TO AGENT',
                          '04 JOB VIEWED BY AGENT',
                          '05 FIELD CALL REQUIRED',
                          '34 PACKAGING SENT',
                          '39 CX NOT AVAIL - MESSAGE LEFT',
                          '40 CX NOT AVAIL - NO RESPONSE',
                          '41 CX NOT AVAIL - WRONG NUM',
                          '06 FIELD CALL ARRANGED',
                          '30 OUT CARD LEFT',
                          '07 UNIT IN REPAIR',
                          '31 WAITING FOR INFO',
                          '32 INFO RECEIVED',
                          '08 ESTIMATE SENT',
                          '09 ESTIMATE ACCEPTED',
                          '10 ESTIMATE REFUSED',
                          '11 PARTS REQUIRED',
                          '12 PARTS ORDERED',
                          '13 PARTS RECEIVED',
                          '33 PART NO LONGER AVAILABLE',
                          '14 2ND FIELD CALL REQ.',
                          '15 2ND FIELD CALL BKD.',
                          '16 AUTHORITY REQUIRED',
                          '17 AUTHORITY ISSUED',
                          '18 FIELD CALL COMPLETE',
                          '18 REPAIR COMPLETED',
                          '19 DELIVER BACK REQ.',
                          '20 DELIVER BACK ARR.',
                          '21 E-INVOICE SENT',
                          '22 E-INVOICE RECEIVED',
                          '23 E-INVOICE REJECTED',
                          '25 INVOICE PAID',
                          '26 INVOICE RECONCILED',
                          '27 JOB ARCHIVED',
                          '24 RESUBMIT E-INVOICE',
                          '28 RESUBMIT CLAIM',
                          '21A E-INVOICE PENDING',
                          '29 JOB CANCELLED',
                          '21C INVOICED',
                          '21W CLAIMED',
                          '35 EINVOICE PENDING',
                          '36 EINVOICE REJECTED',
                          '37 EINVOICE RESUBMITTED',
                          '38 EINVOICE APPROVED',                       /* Added New Status in Status for 5 missing status  */
                          '07 AWAITING REPAIR',
                          '16 AUTHORITY QUERY',
                          '17 AUTHORITY REJECTED',
                          '18A ALTERNATIVE RESOLUTION',
                          '29A JOB CANCELLED - NO CONTACT'
                         );
        
        $jobs = $job_model->fetchJobStatusPeriod($this->config['StatusChangeReport']['DaysReport']);
        $data = $jobs;
        $n = 0;
        
        foreach($jobs as $job) {
            $data[$n]['Number of Parts'] = $job_model->getNumPartsUsedNoAdjustment($job['Skyline No']);
            $data[$n]['Number of Appointments'] = $appointment_model->getNumberForJob($job['Skyline No']);
//            $data[$n]['Number of Contact History Entries'] = $contact_history_model->getNumberForJob($job['Skyline No']);
            $data[$n]['Number of Contact History Entries'] = $job_model->getNumContactHistory($job['Skyline No']);
            
            $sh = $status_history_model->getJobStatusHistory($job['Skyline No']);
            
            foreach($statuses as $status) {
                $data[$n][$status] = '';                                        /* Prepare blank fields */

                $sNo = substr($status,0,strpos($status, ' '));          
                preg_match_all("/\b\w/", substr($status,2), $tla); 
                $sNo .= ' '.implode('', $tla[0]);

                $data[$n]['Date '.$sNo] = '';
                $data[$n]['Time '.$sNo] = '';
                $data[$n]['Count '.$sNo] = 0;
                foreach ($sh as $s) {          
                    if ($s['StatusName'] == $status) {
                        $data[$n][$status] = $status;                           /* The job status */
                        $data[$n]['Date '.$sNo] = date('d/m/Y',  strtotime($s['StatusDate']));
                        $data[$n]['Time '.$sNo] = date('H:i',strtotime($s['StatusDate']));
                        $data[$n]['Count '.$sNo] = $status_history_model->countStatusEntryForJob($job['Skyline No'], $s['StatusName']);
                        break;                                                  /* Have match - don't want to check any more */
                    } /* fi */
                } /* next $s */
            } /* next $status */
            $n++;
        } /* next $job */
        //echo "<pre>";var_export($data);echo "</pre>";
        $csv = $this->array_to_csv($data);          /* Convert to CSV */
        
//        echo $csv;
        
        
        $charset = "utf-8";

        $mail = new PHPMailer(); // defaults to using php "mail()"
       
        $mail->IsSMTP();          // telling the class to use SMTP
        $mail->SMTPAuth   = true; // enable SMTP authentication
        $mail->Host       = $this->config['SMTP']['Host']; 
        $mail->Port       = $this->config['SMTP']['Port']; 
        $mail->Username   = $this->config['SMTP']['Username'];
        $mail->Password   = $this->config['SMTP']['Password'];
        $mail->Timeout = 30;
        
        $filename = 'status_change_report_'.date('Y-m-d').'.csv';
        //$mail->AddStringAttachment($csv, $filename);
        file_put_contents('reports/'.$filename, $csv);
        
        $protocol = 'http';
	if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') {
		$protocol = 'https';
	}
	$host = $_SERVER['HTTP_HOST'];
	$baseUrl = $protocol . '://' . $host;
	if (substr($baseUrl, -1)=='/') {
		$baseUrl = substr($baseUrl, 0, strlen($baseUrl)-1);
	}
        $body = "The Skyline Status Change Report for ".date('d M Y')." can be found at <a href=$baseUrl/reports/$filename>$baseUrl/reports/$filename</a>\n";
        $altbody = "The Skyline Status Change Report for ".date('d M Y')." can be found at $baseUrl/reports/$filename\n";

        $mail->AddAddress( $this->config['StatusChangeReport']['ReportEmail']);
        $mail->From       = $this->config['General']['skyline_issue_email'];
        $mail->FromName   = $this->config['General']['skyline_issue_email'];
        $mail->Subject    = "Skyline Status Change Report ".date('Y-m-d');
        $mail->AltBody = $altbody;
        $mail->MsgHTML($body);

        if(!$mail->Send())
        echo "Could not send mail!<br />\n{$mail->ErrorInfo}";
        else
        echo "Mail sent\n";
    }
    
    
    
    /**
    * Generatting CSV formatted string from an array.
    * By Sergey Gurevich.
    ****************************************************************************/
    private function array_to_csv($array, $header_row = true, $col_sep = ",", $row_sep = "\n", $qut = '"')
    {
        if (!is_array($array) or !is_array($array[0])) return false;
        $output = "";
        //Header row.
        if ($header_row)
        {
            foreach ($array[0] as $key => $val)
            {
                //Escaping quotes.
                $key = str_replace($qut, "$qut$qut", $key);
                $output .= "$col_sep$qut$key$qut";
            }
            $output = substr($output, 1)."\n";
        }
        //Data rows.
        foreach ($array as $key => $val)
        {
            $tmp = '';
            foreach ($val as $cell_key => $cell_val)
            {
                //Escaping quotes.
                $cell_val = str_replace($qut, "$qut$qut", $cell_val);
                $tmp .= "$col_sep$qut$cell_val$qut";
            }
            $output .= substr($tmp, 1).$row_sep;
        }

        return $output;
    }

    
    
    /**
    Outputs network clients in JSON format
    2012-01-23 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getNetworkClientsAction() {
	
	$networkID = $_REQUEST["networkID"];
	$clientModel = $this->loadModel("Clients");
	$clients = $clientModel->getNetworkClients($networkID);
	echo(json_encode($clients));
	
    }

     public function getModelsListAction($args) {
        $ManufacturerID = $args["ManufacturerID"];
        //$this->log($ManufacturerID );
        $selectedModels=$this->loadModel('Models');
        $modelsList = $selectedModels->getManufacturerModels($ManufacturerID);       
        echo json_encode($modelsList);
     }
    
    public function getClientBranchesAction() {
	
	$clientID = $_REQUEST["clientID"];
	$skylineModel = $this->loadModel("Skyline");
	$branches = $skylineModel->getClientBranches($clientID);
	echo(json_encode($branches));
	
    }


    
    /**
    Outputs client's bought out guarantee data in JSON format
    2012-01-23 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getBoughtOutGuaranteeDataAction() {
	
	$guaranteeModel = $this->loadModel("Guarantee");
	$data = $guaranteeModel->getBoughtOutGuaranteeData($_REQUEST["clientID"]);
	echo(json_encode(["aaData" => $data]));
	
    }
    
    
    /*
     * Update by Praveen Kumar N
     */
    
    public function isImeiRequiredAction() {
	
	$unitModel = $this->loadModel("UnitTypes");
	//$result = $unitModel->isImeiRequired($_POST["unitTypeID"]);
	$result = $unitModel->isImeiRequired($_POST["unitTypeID"], $_POST["modelName"]);
	if(count($result)>0)
        {
            
        }
        else
        {
            $result = false;
        }
	
	echo(json_encode($result));
	
	
    }
    
    
    
    public function getUnitTypeAccessoriesAction() {
	
	$unitModel = $this->loadModel("UnitTypes");
	$result = $unitModel->fetchAccessories($_POST["unitTypeID"]);
        
        if(count($result)>0)
        {
            
        }
        else
        {
            $result = false;
        }
        
	echo json_encode($result);
	
    }

    
    /**
    @action gets closed jobs in the logged in user's scope
    @input  void
    @output JSON
    @return void
    2013-03-18 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getClosedJobsAction($args) {

        $this->page =  $this->messages->getPage($this->session->mainPage, 'en');
        
        $users_model = $this->loadModel('Users');
        $gaugePrefs = $users_model->getGaugePreferences( $this->user->UserID );
        
        $SkylineModel = $this->loadModel('Skyline');
        $completionStatusResult = $SkylineModel->getCompletionStatuses();
        foreach($completionStatusResult as $ini=>$ival){
            $gp['id'][] = $ival['CompletionStatusID'];
            $gp['name'][] = $ival['CompletionStatusName'];
            $completionStatus[$ival['CompletionStatusID']] = $ival['CompletionStatusName'];
        }
        
        if(count($gaugePrefs) == 0){
            $gaugePrefs = array(
                'GaDial0StatusID' => $gp['id'][0],
                'GaDial0Status' => $gp['name'][0],
                'GaDial0Yellow' => 0,
                'GaDial0Red' => 0,
                'GaDial1StatusID' => $gp['id'][1],
                'GaDial1Status' => $gp['name'][1],
                'GaDial1Yellow' => 0,
                'GaDial1Red' => 0,
                'GaDial2StatusID' => $gp['id'][2],
                'GaDial2Status' => $gp['name'][2],
                'GaDial2Yellow' => 0,
                'GaDial2Red' => 0,
                'GaDial3StatusID' => $gp['id'][3],
                'GaDial3Status' => $gp['name'][3],
                'GaDial3Yellow' => 0,
                'GaDial3Red' => 0,
                'GaDial4StatusID' => $gp['id'][4],
                'GaDial4Status' => $gp['name'][4],
                'GaDial4Yellow' => 0,
                'GaDial4Red' => 0,
                'GaDial5StatusID'=>0,
                'GaDial5Yellow' => 0,
                'GaDial5Red' => 0
            );
        }
        
	$Job = $this->loadModel('Job');
	
	$jobModel = $this->loadModel("Job");
	$result = $jobModel->getClosedJobs(array_merge($args,$_GET),$this->user->UserID,$this->page['config']['pageID'],$gaugePrefs,$completionStatus);
	echo(json_encode($result));
	
    }
    
    /**
    @action gets closed jobs summary in the logged in user's scope
    @input  void
    @output JSON
    @return void
    2013-03-18 © Vic <v.rutkunas@pccsuk.com>
    */
    
    public function getClosedJobsSummaryAction($args) {

        $this->page =  $this->messages->getPage($this->session->mainPage, 'en');
        
        $users_model = $this->loadModel('Users');
        $gaugePrefs = $users_model->getGaugePreferences( $this->user->UserID );
        
        $SkylineModel = $this->loadModel('Skyline');
        $completionStatusResult = $SkylineModel->getCompletionStatuses();
        foreach($completionStatusResult as $ini=>$ival){
            $gp['id'][] = $ival['CompletionStatusID'];
            $gp['name'][] = $ival['CompletionStatusName'];
            $completionStatus[$ival['CompletionStatusID']] = $ival['CompletionStatusName'];
        }
        
        if(count($gaugePrefs) == 0){
            $gaugePrefs = array(
                'GaDial0StatusID' => $gp['id'][0],
                'GaDial0Status' => $gp['name'][0],
                'GaDial0Yellow' => 0,
                'GaDial0Red' => 0,
                'GaDial1StatusID' => $gp['id'][1],
                'GaDial1Status' => $gp['name'][1],
                'GaDial1Yellow' => 0,
                'GaDial1Red' => 0,
                'GaDial2StatusID' => $gp['id'][2],
                'GaDial2Status' => $gp['name'][2],
                'GaDial2Yellow' => 0,
                'GaDial2Red' => 0,
                'GaDial3StatusID' => $gp['id'][3],
                'GaDial3Status' => $gp['name'][3],
                'GaDial3Yellow' => 0,
                'GaDial3Red' => 0,
                'GaDial4StatusID' => $gp['id'][4],
                'GaDial4Status' => $gp['name'][4],
                'GaDial4Yellow' => 0,
                'GaDial4Red' => 0,
                'GaDial5StatusID'=>0,
                'GaDial5Yellow' => 0,
                'GaDial5Red' => 0
            );
        }
        
       
        $TatModel   = $this->loadModel('TAT');
        $TatResult  = $TatModel->fetchRow(array('TatType'=>'ClosedJobs'));
     
	$Job = $this->loadModel('Job');
	
	$jobModel = $this->loadModel("Job");
	$result = $jobModel->getClosedJobsSummary(array_merge($args,$_GET),$this->user->UserID,$this->page,$TatResult,$gaugePrefs,$completionStatus);
	echo(json_encode($result));
	
    }
    
    
    /**
     * updateCurrencyExchange
     *  
     * Return the record associated with a specific Skillset ID
     * 
     * @param 
     * 
     * @return      
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function updateCurrencyExchangeAction() {
        $currency_rate_model = $this->loadModel('CurrencyRate');
        
        $currency_rate_model->UpdateFromEcbXml($this->config['CurrencyExchange']['EcbXml']);
    }
    
    /* Test currency rate ie {skylinedomain}/Data/testCurrencyRate/25/GBP/USD */
    public function testCurrencyRateAction($args) {
        $currency_rate_model = $this->loadModel('CurrencyRate');
         echo '<pre>';
        var_export($currency_rate_model->convert(floatval($args[0]), $args[1], $args[2]));
        echo '</pre>';
    }
        
    /**
     * removeOldViamenteMatrix
     *  
     * Remove the old Viamente Optiomised Matrices which are dated yeterday or before
     * 
     * @param 
     * 
     * @return      
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/  
    public function removeOldViamenteMatrix() {
        $diary_matrix_model = $this->loadModel('DiaryMatrix');
        
        $dt = date("Y-m-d", strtotime("-1 day"));
        
        $diary_matrix_model->DeleteAllOnAndBefore($dt);
    }

    
    /**
     * resendFailedRma
     *  
     * Find jobs with a failed 
     * 
     * @param 
     * 
     * @return      
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/

    public function resendFailedRmaAction() {
        $api_jobs_model = $this->loadModel('APIJobs');                          /* Required for RMA client call */
        $job_model = $this->loadModel('Job');                                   /* Required for getting list of failed jobs */
        
        $jIds = $job_model->getJobsFailedRmaSend();                             /* Get the list of jobs which did not send to RMA */

        $this->log('START Job Resend to RMA','RMASendFail_');
        if ( ! is_null($jIds) ) {                                               /* If we have Job Ids returned */
            foreach($jIds as $jId) {                                            /* Loop through each job */
                $result = $api_jobs_model->rmaPutJobDetails($jId['JobID']);     /* Send each job to RMA */

                // Confusing ?!*? rmaPutJobDetails is inconsistent in the returned data type!!!  groan....
                if (is_string($result) && $result == 'SC0001') {                                      /* If success */
                    $this->log("{$jId['JobID']}    Success",'RMASendFail_');    /* Log it */
                } else {
                    if (is_string($result)) {
                        $this->log("{$jId['JobID']}    Error {$result}",'RMASendFail_');       /* Otherwise log error */
                    } else {
                        $this->log("{$jId['JobID']}    Error {$result['response']}",'RMASendFail_');       /* Otherwise log error */
                    }
                }
            } /* next $jId */
        } /* fi ! isnull $jids */
        $this->log('END Job Resend to RMA','RMASendFail_');
    }
}

?>
