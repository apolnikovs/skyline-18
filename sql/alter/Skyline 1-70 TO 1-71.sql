# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.1.2                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-08-15 17:26                                #
# ---------------------------------------------------------------------- #

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` DROP FOREIGN KEY `network_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `branch_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `client_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_provider_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `product_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `service_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `manufacturer_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `job_type_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `status_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `model_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job`;

ALTER TABLE `job` DROP FOREIGN KEY `user_TO_job_ModifiedUser`;

ALTER TABLE `audit` DROP FOREIGN KEY `job_TO_audit`;

ALTER TABLE `part` DROP FOREIGN KEY `job_TO_part`;

ALTER TABLE `appointment` DROP FOREIGN KEY `job_TO_appointment`;

ALTER TABLE `contact_history` DROP FOREIGN KEY `job_TO_contact_history`;

ALTER TABLE `status_history` DROP FOREIGN KEY `job_TO_status_history`;

# ---------------------------------------------------------------------- #
# Modify table "job"                                                     #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD COLUMN `RMANumber` INTEGER;

ALTER TABLE `job` ADD COLUMN `AgentStatus` VARCHAR(30);

# ---------------------------------------------------------------------- #
# Add table "version"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `version` (
    `VersionNo` VARCHAR(10) NOT NULL,
    `Updated` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT `versionID` PRIMARY KEY (`VersionNo`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "postcode"                                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `postcode` (
    `Postcode` VARCHAR(4) NOT NULL,
    `CountyCode` INTEGER,
    CONSTRAINT `postcodeID` PRIMARY KEY (`Postcode`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "doc_api"                                                    #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_api` (
    `DocApiID` INTEGER NOT NULL AUTO_INCREMENT,
    `Name` VARCHAR(50) NOT NULL,
    `Version` VARCHAR(10) NOT NULL,
    `Author` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Baseuri` VARCHAR(50) NOT NULL,
    `TimeStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted` TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (`DocApiID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

# ---------------------------------------------------------------------- #
# Add table "doc_categories"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_categories` (
    `DocCategoriesID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocApiID` INTEGER NOT NULL,
    `Name` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Path` VARCHAR(50) NOT NULL,
    `TimeStamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `deleted` TINYINT(4) NOT NULL DEFAULT 0,
    PRIMARY KEY (`DocCategoriesID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_categories_DocApiID_FK` ON `doc_categories` (`DocApiID`);

# ---------------------------------------------------------------------- #
# Add table "doc_method_response"                                        #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_method_response` (
    `DocMethodResponseID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocProceduresID` INTEGER NOT NULL,
    `AnswerCode` INTEGER NOT NULL,
    `Response` TEXT NOT NULL,
    `Description` TEXT,
    `CDATA` TEXT,
    PRIMARY KEY (`DocMethodResponseID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_method_response_DocProceduresID_FK` ON `doc_method_response` (`DocProceduresID`);

# ---------------------------------------------------------------------- #
# Add table "doc_procedures"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_procedures` (
    `DocProceduresID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocCategoriesID` INTEGER NOT NULL,
    `Name` VARCHAR(50) NOT NULL,
    `Description` TEXT NOT NULL,
    `Path` VARCHAR(50),
    `methods` VARCHAR(100) NOT NULL,
    `formats` VARCHAR(100) NOT NULL,
    `auth_types` VARCHAR(100),
    `DocApiID` INTEGER,
    PRIMARY KEY (`DocProceduresID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_procedures_DocCategoriesID_FK` ON `doc_procedures` (`DocCategoriesID`);

# ---------------------------------------------------------------------- #
# Add table "doc_procedure_parameters"                                   #
# ---------------------------------------------------------------------- #

CREATE TABLE `doc_procedure_parameters` (
    `DocProcedureParametersID` INTEGER NOT NULL AUTO_INCREMENT,
    `DocProceduresID` INTEGER NOT NULL,
    `CodeLanguage` VARCHAR(50) NOT NULL,
    `CDATA` TEXT NOT NULL,
    `Default` VARCHAR(50) NOT NULL,
    `Option` VARCHAR(50) NOT NULL,
    `Type` VARCHAR(50),
    `Size` INTEGER,
    `Example` TEXT NOT NULL,
    `response_ex` TEXT NOT NULL,
    `Required` TINYINT(1) NOT NULL,
    PRIMARY KEY (`DocProcedureParametersID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_doc_procedure_parameters_DocProceduresID_FK` ON `doc_procedure_parameters` (`DocProceduresID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `job` ADD CONSTRAINT `network_TO_job` 
    FOREIGN KEY (`NetworkID`) REFERENCES `network` (`NetworkID`);

ALTER TABLE `job` ADD CONSTRAINT `branch_TO_job` 
    FOREIGN KEY (`BranchID`) REFERENCES `branch` (`BranchID`);

ALTER TABLE `job` ADD CONSTRAINT `client_TO_job` 
    FOREIGN KEY (`ClientID`) REFERENCES `client` (`ClientID`);

ALTER TABLE `job` ADD CONSTRAINT `service_provider_TO_job` 
    FOREIGN KEY (`ServiceProviderID`) REFERENCES `service_provider` (`ServiceProviderID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `job` ADD CONSTRAINT `product_TO_job` 
    FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`);

ALTER TABLE `job` ADD CONSTRAINT `service_type_TO_job` 
    FOREIGN KEY (`ServiceTypeID`) REFERENCES `service_type` (`ServiceTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `manufacturer_TO_job` 
    FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`);

ALTER TABLE `job` ADD CONSTRAINT `job_type_TO_job` 
    FOREIGN KEY (`JobTypeID`) REFERENCES `job_type` (`JobTypeID`);

ALTER TABLE `job` ADD CONSTRAINT `status_TO_job` 
    FOREIGN KEY (`StatusID`) REFERENCES `status` (`StatusID`);

ALTER TABLE `job` ADD CONSTRAINT `model_TO_job` 
    FOREIGN KEY (`ModelID`) REFERENCES `model` (`ModelID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job` 
    FOREIGN KEY (`BookedBy`) REFERENCES `user` (`UserID`);

ALTER TABLE `job` ADD CONSTRAINT `user_TO_job_ModifiedUser` 
    FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`);

ALTER TABLE `doc_categories` ADD CONSTRAINT `doc_api_TO_doc_categories` 
    FOREIGN KEY (`DocApiID`) REFERENCES `doc_api` (`DocApiID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_method_response` ADD CONSTRAINT `doc_procedures_TO_doc_method_response` 
    FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_procedures` ADD CONSTRAINT `doc_categories_TO_doc_procedures` 
    FOREIGN KEY (`DocCategoriesID`) REFERENCES `doc_categories` (`DocCategoriesID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `doc_procedure_parameters` ADD CONSTRAINT `doc_procedures_TO_doc_procedure_parameters` 
    FOREIGN KEY (`DocProceduresID`) REFERENCES `doc_procedures` (`DocProceduresID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `audit` ADD CONSTRAINT `job_TO_audit` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `part` ADD CONSTRAINT `job_TO_part` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `appointment` ADD CONSTRAINT `job_TO_appointment` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `contact_history` ADD CONSTRAINT `job_TO_contact_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `status_history` ADD CONSTRAINT `job_TO_status_history` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

# ---------------------------------------------------------------------- #
# Repair/add procedures                                                  #
# ---------------------------------------------------------------------- #

DROP PROCEDURE IF EXISTS `UpgradeSchemaVersion`;
 DELIMITER //
CREATE PROCEDURE `UpgradeSchemaVersion`  
	(p_version varchar(20))
BEGIN
    DECLARE bad_version CONDITION FOR SQLSTATE '99001';
    DECLARE current_version varchar(20);
    SELECT `VersionNo` from `version` order by `Updated` desc limit 0,1 into current_version;
    IF p_version <> current_version THEN
         SIGNAL bad_version
            SET MESSAGE_TEXT='Cannot Upgrade Schema.';
    END IF;
END;//
DELIMITER ;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.71');
