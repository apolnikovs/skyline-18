# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-10-25 09:34                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.105');

# ---------------------------------------------------------------------- #
# Modify table "Contact_Us_Subject"                                      #
# ---------------------------------------------------------------------- #

RENAME TABLE `Contact_Us_Subject` TO `contact_us_subject_temp`;
RENAME TABLE `Contact_Us_Subject_temp` TO `contact_us_subject`;

# ---------------------------------------------------------------------- #
# Modify table "Contact_Us_Messages"                                     #
# ---------------------------------------------------------------------- #

RENAME TABLE `Contact_Us_Messages` TO `contact_us_messages_temp`;
RENAME TABLE `Contact_Us_Messages_temp` TO `contact_us_messages`;

# ---------------------------------------------------------------------- #
# Modify table "Welcome_Message_Defaults"                                #
# ---------------------------------------------------------------------- #

RENAME TABLE `Welcome_Message_Defaults` TO `welcome_message_defaults_temp`;
RENAME TABLE `Welcome_Message_Defaults_temp` TO `welcome_message_defaults`;

# ---------------------------------------------------------------------- #
# Modify table "Welcome_Messages"                                        #
# ---------------------------------------------------------------------- #

RENAME TABLE `Welcome_Messages` TO `welcome_messages_temp`;
RENAME TABLE `Welcome_Messages_temp` TO `welcome_messages`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.106');
