<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Retailers Page 
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class Retailers extends CustomModel {
    
    private $conn;
    private $dbColumns = ['RetailerID', "RetailerName", 'Status'];
    private $table     = "retailer";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
          if($args['firstArg']!='')
          {
              $args['where'] =  "BranchID=".$this->conn->quote($args['firstArg']); 
              
          }
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['RetailerID']) || !$args['RetailerID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate name.
     *
     * @param interger $RetailerName  
     * @param interger $BranchID
     * @param interger $RetailerID 
     * 
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($RetailerName, $BranchID, $RetailerID) {
        
         
         
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT RetailerID FROM '.$this->table.' WHERE RetailerName=:RetailerName AND BranchID=:BranchID AND RetailerID!=:RetailerID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':RetailerName' => $RetailerName, ':BranchID' => $BranchID,  ':RetailerID' => $RetailerID));
        
         
         
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['RetailerID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to insert data into database.
    *
    * @param array $args  
    * @global $this->table 
    * @return array It contains status of operation and message.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function create($args) {
        
        if($this->isValidAction($args['RetailerName'], $args['BranchID'], 0)) {

	    $sql = 'INSERT INTO	' . $this->table . ' 
				(
				    RetailerName,
				    BranchID,
				    Status, 
				    CreatedDate, 
				    ModifiedUserID, 
				    ModifiedDate
				)
		    VALUES
				(
				    :RetailerName,
				    :BranchID, 
				    :Status, 
				    :CreatedDate, 
				    :ModifiedUserID, 
				    :ModifiedDate
				)
		';
        
            $insertQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            
            $insertQuery->execute([
		":RetailerName" => $args["RetailerName"],
                ':BranchID' => ($args['BranchID'] != '') ? $args['BranchID'] : NULL, 
                ':Status' => $args['Status'],
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")
	    ]);
        
	    return [
		'status' => 'OK',
		'message' => 'Your data has been inserted successfully.'
	    ];
	    
        } else {
            
            return [
		'status' => 'ERROR',
		'message' => 'Error has been occurred.'
	    ];
	    
        }
	
    }
    
    
    
    /**
    * Description
    * 
    * This method is used for to fetch a row from database.
    *
    * @param array $args
    * @global $this->table  
    * @return array It contains row of the given primary key.
    * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
    */ 
    
    public function fetchRow($args) {
	
        
        if(isset($args['RetailerName']) && isset($args['BranchID']))
        {
            $sql = 'SELECT  RetailerID, 
                            RetailerName, 
                            BranchID,
                            Status, 
                            EndDate 

                    FROM	' . $this->table . ' 

                    WHERE   BranchID = :BranchID AND RetailerName=:RetailerName
                    ';

            $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

            $fetchQuery->execute([':RetailerName' => $args['RetailerName'], ':BranchID' => $args['BranchID']]);
            $result = $fetchQuery->fetch();
            return $result;
        }   
        else if(isset($args['RetailerID']) && $args['RetailerID'])
        {
            $sql = 'SELECT  RetailerID, 
                            RetailerName, 
                            BranchID,
                            Status, 
                            EndDate 

                    FROM	' . $this->table . ' 

                    WHERE	RetailerID = :RetailerID
                    ';

            $fetchQuery = $this->conn->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);

            $fetchQuery->execute([':RetailerID' => $args['RetailerID']]);
            $result = $fetchQuery->fetch();
            return $result;
        }
        else
            return false;
    }
    
    
    
    /**
     * Description
     * 
     * This method is used for to fetch all Retailers for given branch id.
     *
     * @param int $BranchID default false
     * @param string $fieldList default 'RetailerID, RetailerName'
     * @param string $startWith 
     * 
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchAll($BranchID=false, $fieldList='RetailerID, RetailerName', $startWith=false) {
        
        
        if($BranchID)
        {    
            if($startWith)
            { 
                $sql        = 'SELECT '.$fieldList.' FROM '.$this->table.' WHERE RetailerName LIKE :RetailerName AND BranchID=:BranchID AND Status=:Status ORDER BY RetailerName';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':RetailerName' => '%'.$startWith.'%', ':BranchID' => $BranchID, ':Status' => 'Active'));
            }
            else
            {
                $sql        = 'SELECT '.$fieldList.' FROM '.$this->table.' WHERE BranchID=:BranchID AND Status=:Status ORDER BY RetailerName';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':BranchID' => $BranchID, ':Status' => 'Active'));
            }
            
        }
        else
        {    
            if($startWith)
            { 
                $sql        = 'SELECT '.$fieldList.' FROM '.$this->table.' WHERE RetailerName LIKE :RetailerName AND Status=:Status ORDER BY RetailerName';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':RetailerName' => '%'.$startWith.'%', ':Status' => 'Active'));
            }
            else
            {
                $sql        = 'SELECT '.$fieldList.' FROM '.$this->table.' WHERE Status=:Status ORDER BY RetailerName';
                $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
                $fetchQuery->execute(array(':Status' => 'Active'));
            }
        }
        
        $result = $fetchQuery->fetchAll();
        return $result;
    }
    
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['RetailerName'], $args['BranchID'], $args['RetailerID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($args['Status']=="In-active")
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                        $EndDate = $row_data['EndDate'];
                }
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = '	UPDATE	' . $this->table . ' 
			
			SET	RetailerName = :RetailerName,
				BranchID = :BranchID, 
				Status = :Status, 
				EndDate = :EndDate, 
				ModifiedUserID = :ModifiedUserID, 
				ModifiedDate = :ModifiedDate

			WHERE	RetailerID = :RetailerID
		     ';
        
	    $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	    $updateQuery->execute([
		"RetailerName" => $args["RetailerName"],
		':BranchID' => ($args['BranchID']!='')?$args['BranchID']:NULL, 
		':Status' => $args['Status'],
		':EndDate' => $EndDate,
		':ModifiedUserID' => $this->controller->user->UserID,
		':ModifiedDate' => date("Y-m-d H:i:s"),
		':RetailerID' => $args['RetailerID']
             ]);
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => 'Error has been occurred.');
        }
    }
    
    
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
}
?>